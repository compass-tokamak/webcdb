import tornado
import tornado.wsgi
import tornado.httpserver

import main

def run(port=8080):
    container = tornado.wsgi.WSGIContainer(main.application)
    http_server = tornado.httpserver.HTTPServer(container)
    http_server.listen(port)
    tornado.ioloop.IOLoop.current().start()

if __name__ == "__main__":
    run()