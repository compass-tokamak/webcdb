"""
Configuration for WebCDB

This file contains various configuration settings.
"""
# TODO: Maybe this should be integrated into web.config?
import os, os.path

cache = False

# Path where the local repository is stored.
postproc_repository_path = os.getenv("postproc_local_path")

# The remote repository to which we push and we pull from.
postproc_push_path = os.getenv("postproc_remote_path")

#templates_dir = os.path.join(os.getcwd(),'templates')
templates_dir = '/var/www/webcdb/templates/'

