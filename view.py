"""
WebCDB Views

This file defines how templates are rendered.
It also sets all global variables to be understood by templates.

Layouts
-------
- render
- render_plain
"""

import web
import config
import helpers
import auth

from datetime import datetime

# All global variables accessible from templates
t_globals = dict(
    datestr = web.datestr,
    ctx = web.ctx,
)

# Import all helpers from
for helper in dir(helpers):
    if helper.startswith("__"):
        pass
    t_globals[helper] = helpers.__getattribute__(helper)

# Auth helpers
t_globals["get_user"] = auth.get_user

# try to get fqdn
try:
    import socket
    t_globals["fqdn"] = socket.getfqdn()
except Exception:
    t_globals["fqdn"] = ""

render_plain = web.template.render(config.templates_dir, globals=t_globals, cache=config.cache)

def is_ajax():
    return web.ctx.env.get('HTTP_X_REQUESTED_WITH') == "XMLHttpRequest"

t_globals["is_ajax"] = is_ajax

def layout(page):
    if is_ajax():
        return render._template("plain_layout")(page)
    else:
        return render._template("layout")(page)

# Customized renderer that uses layout method
# to choose between clasical "layout" and "plain_layout"
# "plain_layout" is selected for AJAX calls.
render = web.template.render(config.templates_dir, cache=config.cache, 
    globals=t_globals)
render._base = layout
