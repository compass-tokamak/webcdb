"""
Various CDB library functions

"""
from pyCDB import DAQClient
from pyCDB import pyCDBBase
OrderedDict = pyCDBBase.OrderedDict
from math import ceil
from numpy import arange

_client = None
def get_client():
    """ Return a CDB client.

    :rtype DAQClient.CDBDAQClient
    """
    global _client

    if not _client:
        _client = DAQClient.CDBDAQClient()
    return _client

__all__ = [ 'get_client', 'OrderedDict', 'run_sql', 'get_data_slice' ]

def run_sql(sql, client = None):
    """ Run SQL command against the DB and return an array of dicts."""

    reconnect = True
    get_client().db.ping(reconnect)
    return get_client().query(sql)

def get_data_slice(signal, start = None, stop = None, max_size = None, downsample = None):
    """ Take data signal and take a part of it.

    :param start: Minimum time (in ms) to take
    :param stop: Maximum time (in ms) to take
    :param max_size: Maximum size of the resulting data set (will be downsampled)
    :param downsample: Factor by which to downsample
    :returns: (new_time_axis, new_data)

    When downsampling, max_size takes precedence over downsample.
    """
    if hasattr(signal,"time_axis"):
        x = signal.time_axis.data[0:signal.data.shape[0]]
    else:
        x = arange(signal.data.shape[0])
    y = signal.data

    # cropping
    if start is not None:
        selected_indexes = x > start
        x = x[selected_indexes]
        y = y[selected_indexes]
    if stop is not None:
        selected_indexes = x < stop
        x = x[selected_indexes]
        y = y[selected_indexes]

    # downsampling
    if max_size or downsample:
        if max_size:
            downsample = int( x.shape[0] / max_size )
        if downsample > 1:
            x = x[::downsample]
            y = y[::downsample]

    return (x, y)

def get_data_slice_2d(signal, frame = 0, xstart = None, xstop = None, ystart = None, ystop = None,
                      xdownsample = None, ydownsample = None, max_size_x = None, max_size_y = None):
    """ Take data signal and take a part of it.
    :param frame: Number of frame
    :param xstart: Lower bound of x interval
    :param xstop: Upper bound of x interval
    :param ystart: Lower bound of y interval
    :param ystop: Upper bound of y interval
    :param max_size_x: Maximum size of the resulting data set (will be downsampled) in x direction
    :param max_size_y: Maximum size of the resulting data set (will be downsampled) in y direction
    :param xdownsample: Factor by which to downsample in x direction
    :param ydownsample: Factor by which to downsample in y direction
    :returns: (x,y,z) data

    When downsampling, max_size takes precedence over downsample.
    """
    
    z = signal.data[frame,...]
    x = signal.axis2.data
    if len(x.shape) > 1:
        x = x[frame,...]
    y = signal.axis1.data
    if len(y.shape) > 1:
        y = y[frame,...]

    # cropping
    if xstart is not None:
        selected_indexes = x >= xstart
        x = x[selected_indexes]
        z = z[:,selected_indexes]
    if xstop is not None:
        selected_indexes = x < xstop
        x = x[selected_indexes]
        z = z[:,selected_indexes]
    if ystart is not None:
        selected_indexes = y >= ystart
        y = y[selected_indexes]
        z = z[selected_indexes,:]
    if ystop is not None:
        selected_indexes = y < ystop
        y = y[selected_indexes]
        z = z[selected_indexes,:]
        
    # downsampling
    if max_size_x or xdownsample:
        if max_size_x:
            xdownsample = int(ceil(float(z.shape[1]) / max_size_x))
            if xdownsample == 0:
                xdownsample = 1;
    if max_size_y or ydownsample:
        if max_size_y:
            ydownsample = int(ceil(float(z.shape[0]) / max_size_y))
            if ydownsample == 0:
                ydownsample = 1;
    z = z[::ydownsample,::xdownsample]
    x = x[::xdownsample]
    y = y[::ydownsample]
    t = signal.time_axis.data[frame]
    
    return (x,y,z,t)
