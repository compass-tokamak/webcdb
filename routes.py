"""
Routes for WebCDB

All URLs have to be matched by one of the route regexes.
"""
routes = (
    # Home page
    '/', 'models.index.Index',

    '/hardware/?', 'models.computers.Index',
    '/hardware/([0-9a-zA-Z_]+)/?', 'models.computers.Show',
    '/hardware/([0-9a-zA-Z_]+)/([0-9a-zA-Z_]+)/([0-9a-zA-Z_]+)/?', 'models.channels.Show',
    '/hardware/([0-9a-zA-Z_]+)/([0-9a-zA-Z_]+)/([0-9a-zA-Z_]+)/edit_history', 'models.channels.EditHistory',
    '/hardware/([0-9a-zA-Z_]+)/([0-9a-zA-Z_]+)/([0-9a-zA-Z_]+)/edit_parameters', 'models.channels.EditParameters',

    # Login
    '/login/?', 'models.login.Login',
    '/logout/?', 'models.login.Logout',

    # API
    '/api/?', 'models.api.Index',
    '/api/([0-9a-zA-Z_]+)', 'models.api.Execute',

    # Records
    '/records/?', 'models.shots.Index',
    '/records/last?', 'models.shots.Last',
    '/records/(\d+)/?', 'models.shots.Show',
    '/records/(\d+)/boards', 'models.shots.Boards',

    # Data sources
    '/data_sources/?', 'models.data_sources.Index',
    '/data_sources/new/?', 'models.data_sources.New',
    '/data_sources/([0-9a-zA-Z_]+)/?', 'models.data_sources.Show',

    # Data signals
    '/data_signals/(\d+)/(\d+)/?', 'models.data_signals.Show',
    '/data_signals/(\d+)/(\d+)/plot/?' , 'models.data_signals.Plot',
    '/data_signals/(\d+)/(\d+)/viewer/?' , 'models.data_signals.Viewer',
    '/data_signals/(\d+)/(\d+)/plotly_viewer/?' , 'models.data_signals.PlotlyViewer',
    '/data_signals/(\d+)/(\d+)/plotly2d/([0-9a-zA-Z_]+)/?' , 'models.data_signals.Plotly2D',
    '/data_signals/(\d+)/(\d+)/data/?' , 'models.data_signals.Data',
    '/data_signals/(\d+)/(\d+)/edit/?' , 'models.data_signals.Edit',
    '/data_signals/(\d+)/(\d+)/delete/?' , 'models.data_signals.Delete',
    '/data_signals/(\d+)/(\d+)/undelete/?' , 'models.data_signals.Undelete',
    '/data_signals/(\d+)/(\d+)/restore/?' , 'models.data_signals.Restore',
    '/data_signals/%s/(\d+)/?' % "(.*)", 'models.data_signals.Show',   # UNSAFE (string)
    '/data_signals/%s/(\d+)/plot/?' % "(.*)", 'models.data_signals.Plot',   # UNSAFE (string)
    '/data_signals/%s/(\d+)/viewer/?' % "(.*)", 'models.data_signals.Viewer',   # UNSAFE (string)

    # Generic signals
    '/generic_signals/?', 'models.generic_signals.Index', # [?q=search_query]
    '/generic_signals/new', 'models.generic_signals.New',
    '/generic_signals/create', 'models.generic_signals.Create',
    '/generic_signals/(\d+)/?', 'models.generic_signals.ShowById', # by generic_signal_id
    '/generic_signals/(\d+)/update_calibration', 'models.generic_signals.UpdateCalibration',
    '/generic_signals/(\d+)/detach', 'models.generic_signals.Detach',
    '/generic_signals/(\d+)/attach', 'models.generic_signals.Attach',
    '/generic_signals/(\d+)/edit_parameters', 'models.generic_signals.EditParameters',
    '/generic_signals/%s/%s/?' % ("([^/]*)", "([^/]*)"), 'models.generic_signals.ShowByName',

    # Post-processing rules
    '/postprocessing(?:/|/rules/?)?', 'models.postprocessing.Index', 
    '/postprocessing/rules/new', 'models.postprocessing.New', 
    '/postprocessing/rules/(\d+)/?', 'models.postprocessing.Show', 
    '/postprocessing/rules/(\d+)/edit', 'models.postprocessing.Edit', 
    '/postprocessing/rules/(\d+)/deactivate', 'models.postprocessing.Deactivate', 
    '/postprocessing/rules/(\d+)/activate', 'models.postprocessing.Activate', 
    '/postprocessing/rules/(\d+)/source', 'models.postprocessing.Source', 
    '/postprocessing/rules/(\d+)/update_inputs', 'models.postprocessing.UpdateInputs', 
    '/postprocessing/rules/(\d+)/update_outputs', 'models.postprocessing.UpdateOutputs',
    '/postprocessing/run/?', 'models.postprocessing.Run',
    '/postprocessing/records/(\d+)/?', 'models.postprocessing.ShowRecord',
    '/postprocessing/log/?', 'models.postprocessing.Log'
)