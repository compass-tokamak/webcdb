"""
Authorization and Sessions for WebCDB

All http requests are wrapped in `auth_processor`. It requires authentication
for requests from outside and by default allows unauthenticated requests
from local network.

This behaviour can be overridden using `login_required` decorator for specific actions.
These then require authentication even from local network.

Supported authentication schemas:
- Basic
- Negotiate (kerberos)

Attributes:
- session is set from outside (is it a good option?)
"""
import re
import ldap3
import base64
import functools
import web
import config
import os
from ldap3.utils.dn import escape_rdn

# Session expiring works only in non-debug mode!
web.config.session_parameters['timeout'] = 1800 # half hour
web.config.session_parameters['ignore_expiry'] = False
web.config.session_parameters['ignore_change_ip'] = False
web.config.session_parameters['secret_key'] = 'dfdfaebjdbaqbkugbiJ'

from web.session import DiskStore
# class DiskStore(web.session.DiskStore):
#     '''Custom disk store.

#     A patch that refreshes session (useful until web.py is updated).'''
#     def __getitem__(self, key):
#         path = self._get_path(key)
#         if os.path.exists(path):
#             os.utime(path, None)
#             pickled = open(path).read()
#             # return self.decode(pickled)
#             return pickled
#         else:
#             raise KeyError(key)

class Session(web.session.Session):
    '''Custom session management class.

    It doesn't interrupt browsing but re-creates a new session instead.'''
    def expired(self):
        self.session_id = None

    def push_message(self, text, level="success"):
        if not '_messages' in self:
            self._messages = list()
        self._messages.append({
            "level" : level,
            "text" : text
        })

    def pop_messages(self):
        if not '_messages' in self:
            return list()
        else:
            messages = self._messages
            self._messages = list()
            return messages


def auth_processor(f, *args, **kwargs):
    """Processor that adds authentication for all requests.

    It requires authentication for all requests that don't come from local network.
    """
    if not is_local():
        if not authenticate():
            return send_unauthorized()
    return f(*args, **kwargs)

def login_required(f):
    """Decorator that requires login for an action."""
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        if authenticate():
            return f(*args, **kwargs)
        else:
            session.redir_path = web.ctx.fullpath
            raise web.seeother('/login')
    return wrapper

def get_user():
    """User name if logged in.

    :rtype: string or None
    """
    return session.get("user", None)

def is_local():
    """Does the request come from local network (or localhost)?"""
    allowed = ["^10.", "^192.", "^127."]
    for pattern in allowed:
        if re.match(pattern, web.ctx.ip):
            return True
    return False

def send_unauthorized():
    """Tell the browser to authenticate user."""
    web.header('WWW-Authenticate', 'Basic realm="WebCDB"')
    # web.header('WWW-Authenticate', 'Negotiate')
    web.ctx.status = '401 Unauthorized'
    return "Unauthorized"

def authenticate():
    """Tries to authenticate user.

    :rtype: bool
    :return: True if authenticated, False if not.
    """
    if session.get("user", None):
        return True
    # Kerberos authentication
    elif "WEBCDB_NO_AUTHENTICATION" in os.environ:
        return True
    elif "REMOTE_USER" in web.ctx.env:
        session.user = web.ctx.env["REMOTE_USER"]
        return True
    # Basic HTTP authentication
    elif 'HTTP_AUTHORIZATION' in web.ctx.env:
        auth = web.ctx.env['HTTP_AUTHORIZATION']
        if re.match('^Negotiate', auth):
            print("Negotiate")
            if is_local():
                raise Exception("Invalid Kerberos token")
            else:
                return False

        auth = re.sub('^Basic ', '', auth)
        username, password = base64.decodestring(bytes(auth, 'utf-8')).decode().split(':')
        # Just for sure escape username
        username = escape_rdn(username)
        user_id = "uid={},ou=People,dc=compass".format(username)
        try:
            print("Checking LDAP...")
            # ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_ALLOW) # Turn off certificate checking!
            serv = ldap3.Server("ldaps://ldap")
            con = ldap3.Connection(serv, user=user_id, password=password)
            if con.bind():
                print('Login "{}" valid'.format(username))
                con.unbind()
                session.user = username
                return True
            else:
                print('Login "{}" not valid'.format(username))
                con.unbind()
                return False
        except:
            pass
    return False
