$(function() {
    var $element = $(".json-editor");

    if ($element.length) {
        var text = $element.text();
        $element.text("");

        if (!text) {
            text = "{}";
        }
        var json = JSON.parse(text);
        var options = {
            mode : 'tree',
            modes : ['code', 'tree'],
            error: function (err) {
                alert(err.toString());
            }
        };

        var editor = new jsoneditor.JSONEditor($element[0], options, json);
    }

    var $form = $(".edit-parameters-form");
    $form.on("submit", function() {
        var json = editor.get();
        var str = JSON.stringify(json, null, 2);
        $("#parameters-hidden").val(str);
        return true;
    });

    $(".load-button").click(function() {
        var $button = $(this);
        var $pre = $button.closest(".panel").find("pre");
        var text = $pre.text();
        if (!text) {
            text = "{}";
        }
        editor.set(JSON.parse(text));
    });
})
