$(function() {
    var $sourceHidden = $("#source-hidden");
    var $saveButton = $("#save-button");

    // ACE editor
    var editor = ace.edit("source-edit");
    editor.setTheme("ace/theme/github");
    editor.getSession().setMode("ace/mode/python");
    editor.getSession().setUseSoftTabs(true);
    editor.setShowPrintMargin(false);    

    editor.getSession().on('change', function(e) {
        $saveButton.show();
        $sourceHidden.val(editor.getValue());
    });
});