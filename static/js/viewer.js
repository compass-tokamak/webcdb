/**
  * Suport javascript for rescaling in viewer.
  */
$(function() {
    var width = 1024;
    var height = 768;

    var svgPlotData = function(data) {
        $("#graph").empty();

        /* implementation heavily influenced by http://bl.ocks.org/1166403 */
        
        // define dimensions of graph
        var m = [10, 10, 30, 80]; // margins
        var w = width - m[1] - m[3]; // width
        var h = height - m[0] - m[2]; // height  

        // Accessors
        var getX = function(d) { return d[0]};
        var getY = function(d) { return d[1]};
        
        // Calculate extreme values
        var minX = d3.min(data, getX);
        var minY = d3.min(data, getY);
        var maxX = d3.max(data, getX);
        var maxY = d3.max(data, getY);  

        // Solve ranges for trivial data (1 value over the entire range)
        if (minY == maxY) {
            minY--; maxY++;
        }

        // Create scales (exact for x, a bit taller for y)
        var x = d3.scale.linear().nice().domain([minX, maxX]).range([0, w]);
        var y = d3.scale.linear().nice().domain([minY - (maxY - minY) / 20, maxY + (maxY - minY) / 20]).range([h, 0]);

        // Create a line function that can convert datasets into x and y points
        var lineCoords = d3.svg.line()
            .x(function(d) { 
                return x(d[0]); 
            })
            .y(function(d) { 
                return y(d[1]); 
            });  

        // Add an SVG element with the desired dimensions and margin.
        var graph = d3.select("#graph").append("svg:svg")
                .attr("width", w + m[1] + m[3])
                .attr("height", h + m[0] + m[2])
                .append("svg:g")
                .attr("transform", "translate(" + m[3] + "," + m[0] + ")");
        
        // x axis
        var xAxis = d3.svg.axis().scale(x).tickSize(-h).tickSubdivide(true);
        xAxis.tickFormat(d3.format(".7g"));
        graph.append("svg:g")
              .attr("class", "x axis")
              .attr("transform", "translate(0," + h + ")")
              .call(xAxis);

        // y axis
        var yAxis = d3.svg.axis().scale(y).tickSize(-w).tickSubdivide(true).ticks(4).orient("left");
        yAxis.tickFormat(d3.format(".3e"))
        graph.append("svg:g")
              .attr("class", "y axis")
              .call(yAxis);

        // Zooming in
        var selecting = false;
        d3.select(".x.axis").on("click", function() {
            var position = d3.mouse(this);
            start_x = position[0];
            if (selecting) {
                selecting = false;

                var new_start = x.invert($(".select-box").attr("x"));
                var new_stop = x.invert(position[0]);

                // $("svg .x.axis").css("cursor", "w-resize");
                if (new_start < new_stop) {
                    plot(new_start, new_stop, $("#viewer-format").val());
                }

            } else {
                selecting = true;
                graph.append("rect")
                    .attr("class", "select-box")
                    .attr("width", 10)
                    .attr("height", h)
                    .attr("x", position[0])
                // $("svg .x.axis").css("cursor", "e-resize");
            }
        }).on("mousemove", function() {
            if (selecting) {
                var position = d3.mouse(this);
                d3.select(".select-box")
                    .attr("width", position[0] - $(".select-box").attr("x"));
            }           
        });

        // In case there are nulls, segment the data into multiple sets;
        var dataSegments = function(data) {
            result = [];
            actual_series = [];

            $.each(data, function(_, element) {
                if ( element[1] === null) {
                    if (actual_series.length) {
                        result.push(actual_series);
                        actual_series = [];
                    }
                } else {
                    actual_series.push(element);
                }
            });
            if (actual_series.length) {
                result.push(actual_series);
            }
            return result;
        };

        var segmented = dataSegments( data );

        // Each data segment drawn as separate line (+ optional points)
        $.each(segmented, function(_, segment) {
            // The line plot itself
            graph.append("svg:path").attr("d", lineCoords(segment));

            // Circles with position tooltips
            if (data.length < 200) {
                var tooltip = d3.select("div.data-tooltip");

                $.each(segment, function(_, point) {
                    graph.append("circle")
                        .attr("r", 7)
                        .attr("cx", x(point[0]))
                        .attr("cy", y(point[1]))
                        .on("mouseover", function() {      
                            tooltip.transition()        
                                .duration(200)      
                                .style("opacity", .95);      
                            tooltip .html("x = " + point[0] + "<br/>" + "y = " + point[1])
                                .style("left", (d3.event.pageX + 15) + "px")     
                                .style("top", (d3.event.pageY - 28) + "px");    
                            })                  
                        .on("mouseout", function() {       
                            tooltip.transition()        
                                .duration(500)      
                                .style("opacity", 0);   
                        });
                });
            }
        });  
    }

    var plot = function(start, stop, format) {
        var url = baseUrl + "api/get_data?max_size=" + ( 2048 /* width * 2 */) + "&start=" + start + "&stop=" + stop + "&str_id=" + strId + "[" + format + "]";

        if (parseFloat(stop) <= parseFloat(start)) {
            alert("Cannot plot an interval of negative or zero length.");
        } else {
            $.ajax({
                dataType: "json",
                url: url,
                success: function(data) {
                    svgPlotData(data);
                    var current = zoomHistory.length ? zoomHistory.slice(-1)[0] : null;
                    if (!current || start != current[0] || stop != current[1]) {
                        zoomHistory.push([start, stop]);
                        if (zoomHistory.length == 1) {
                            $("#undo-zoom, #reset").attr("disabled", "disabled");
                        } else {
                            $("#undo-zoom, #reset").removeAttr("disabled");
                        }
                        $("#start").val(start);
                        $("#stop").val(stop);
                        location.hash = "start=" + start + "&stop=" + stop;
                    }
                    $("#loading").hide();
                },
                error: function() {
                    $("#loading").text("Error");
                }
            });
        }
    }

    function replot() {
        var start = $("#start").val() || 0.0;
        var stop = $("#stop").val() || 2000.0;
        var format = $("#viewer-format").val();
        plot(start, stop, format);
    };

    var zoomHistory = [];

    var zoomBack = function() {
        if (zoomHistory.length > 1) {
            // console.log("zooming")
            zoomHistory.pop();
            var coordinates = zoomHistory.pop();
            // console.log(coordinates);
            plot(coordinates[0], coordinates[1], $("#viewer-format").val());
        }
    }

    $("#zoom").click(function() {
        replot();
        return false;
    });

    $("#reset").click(function() {
        zoomHistory = [];
        plot(0, 1000000, $("#viewer-format").val());
        return false;
    }); 

    $("#undo-zoom").click(function() {
        zoomBack();
        return false;
    });

    $("#download").click(function() {
        var start = $("#start").val();
        var stop = $("#stop").val();
        var url = baseUrl + "data_signals/" + genericSignalId + "/" + recordNumber + "/data?start=" + start + "&stop=" + stop + "&revision=" + revision;
        $("#download").attr("href", url);
        return true;
    });

    $("#viewer-format").loadState();

    // Enable loading start and stop values from hash
    var parseHash = function(str) {
       var hash = (str ||'').replace(/^#/,'').split('&'),
           parsed = {};

       for(var i =0,el;i<hash.length; i++ ){
            el=hash[i].split('=')
            parsed[el[0]] = el[1];
       }
       return parsed;
    };
    var parsedLocation = parseHash(location.hash);
    if (parsedLocation.start) {
        $("#start").val(parsedLocation.start)
    }
    if (parsedLocation.stop) {
        $("#stop").val(parsedLocation.stop)
    }

    $("#viewer-format").change(function() {
        replot();
    });

    replot();
});