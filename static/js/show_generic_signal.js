/**
 * Support javascript for the generic signal page.
 */
$(function() {
    $("#attach-cancel").click(function() {
        $attachForm = $("#attach-form");
        $attachForm.hide();
        $("#attach-button").show();
        return false;
    });

    /**
      * Change the fields in the table to editable and display button
      * for changing the calibration of generic signal.
      */
    $("#change-button").click(function() {
        var $form = $("#update-form");
        $form.find("table .to-input").each(function() {
            var $td = $(this).parent();
            var name = $(this).attr("id");
            var value = $(this).text();
            var $input = $("<input>")
                .attr("name", name)
                .attr("id", name)
                .attr("type", "text")
                .addClass("form-control")
                .val(value);
            $td.empty()
                .append($input);
        });

        $("#update-button").show();
    });

    $("#update-form, #attach-form").submit(function() {
        if (
            parseFloat($("#coefficient_lev2V").val()) == 0.0 ||
            parseFloat($("#coefficient_V2unit").val()) == 0.0
        ) {
            return confirm("One of the coefficients is equal to zero (=> so will be the scaled data). Are you sure you want to continue?");
        } else {
            return true;
        }
    });

});