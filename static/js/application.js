// As jQuery plugin
$.fn.enables = function( $element ) {
    var $this = $(this);
    var apply = function() {
        if ($this.is(":checked")) {
            $element.removeAttr("disabled");
        } else {
            $element.attr("disabled", true);
        }
    }
    $this.click(apply);
    apply();
}

// As jQuery plugin
$.fn.shows = function( $element ) {
    var $this = $(this);
    var apply = function() {
        if ($this.is(":checked")) {
            $element.show();
        } else {
            $element.hide();
        }
    }
    $this.click(apply);
    apply();
}

makeUrl = function(appUrl) {
    return ( appUrl[0] === "/" ? "" : "/" ) + $("body").data("homepath") + appUrl;
}

var dialogHtml = '<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
    <div class="modal-dialog">\
      <div class="modal-content">\
        <div class="modal-header">\
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
          <h4 class="modal-title">Title</h4>\
        </div>\
        <div class="modal-body" id="gs-modal-body">\
            Loading...\
        </div>\
        <div class="modal-footer">\
            <button type="button" class="btn btn-ok btn-primary">OK</button>\
        </div>\
      </div>\
    </div>\
</div>'

openDialog = function(title, text, ajaxUrl) {
    var $dialog = $(dialogHtml);
    $("body").append($dialog);
    $dialog.find(".modal-title").html(title);
    var $dialogBody = $dialog.find(".modal-body");
    $dialogBody.html(text);
    if (ajaxUrl) {
        $.ajax(ajaxUrl)
            .done(function(data) {
                $dialogBody.html(data);
            })
            .fail(function() {
                $dialogBody.html("<div class='alert alert-danger'>AJAX Error: dialog content could not be loaded.</alert>");
            })
        // return false;
    }
    $button = $dialog.find(".btn-ok");
    $button.on("click", function() {
        $dialog.modal("hide");
    });    
    $dialog.modal();
    return $dialog;    
}

$(function() {
    // Add help icon to any element with [title] attribute
    $("[title]").tooltip({container: 'body'});

    // Apply shows & enables functionality on elements 
    // with appropriate data-... attributes.
    $("input:radio, input:checkbox").each(function() {
        var $this = $(this);
        var enablesIds = $this.data("enables");

        if (enablesIds) {
            enablesIds = enablesIds.split(",");
            for (var i in enablesIds) {
                var id = enablesIds[i];
                $this.enables($("#" + id));
            }
        }

        var showsIds = $this.data("shows");
        if (showsIds) {
            showsIds = showsIds.split(",");
            for (var i in showsIds) {
                var id = showsIds[i];            
                $this.shows($("#" + id));
            }
        }
    });

    // Request confirmation for dangerous steps
    $(".confirm").on("submit click", function() {
        var message = $(this).data("confirm") || "Do you really want to continue?";
        return confirm(message);
    });

    var attachHelp = function($element, text, url) {
        var $icon = $("<a class='glyphicon glyphicon-question-sign help-icon' title='Show Help'></a>");
        $element.prepend($icon);
        $icon.click(function() {
            openDialog("Help", text, url);
        });
    };

    $("[data-help]").each(function() {
        attachHelp($(this), $(this).data("help"), null);
    });

    $("[data-help-url]").each(function() {
        attachHelp($(this), "Loading...", $(this).data("help-url"));
    });   

    $("[data-help-for]").each(function() {
        $target = $($(this).data("help-for"));
        console.log($target);
        attachHelp($target, $(this).html());
    });

    // If we have a #help-button, it automatically opens the main help
    // for the page (in display:none #help element)
    $("#help-button").on("click", function() {
        openDialog("Help", $("#help").html());
    });

    $("a.inline").on("click", function() {
        var url = $(this).attr("href");
        var title = $(this).attr("title") || $(this).data("original-title") || $(this).text();
        openDialog(title, "Loading...", url);
        return false;
    });
});