$(function() {
	var $buttons = $(".code-toolbar button");

	var show = function(lang) {
		$("pre.code").hide();
		$("pre#code-" + lang).show();
	}

	$buttons.click(function() {
		var lang = $(this).data("lang");
		show(lang);
		$.cookie("lang", lang);
	});
	
	// Selected language
	var value = $buttons.filter(".active").data("lang");
	show(value);
});