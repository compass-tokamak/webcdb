$(function() {
    $("#go-to").submit(function() {
        var template = $(this).data("template");
        var shot = $("#shot").val();
        var url = template.replace("{}", shot);
        window.location = url;
        return false;
    });

    $(".check-board-computer").each(function() {
        var $checkbox = $(this);
        var id = $checkbox.attr("id");
        var className = id.substr(id.indexOf("-") + 1);
        console.log(className);
        console.log($("." + className));
        $checkbox.shows($("." + className));
    });

    $(".ds-tabs a").click(function() {
        $.cookie('selected_data_source', $(this).data("data-source-id"));
    });

    $(".computer-tabs a").click(function() {
        $.cookie('active_computer_id', $(this).data("computer-id"));
    });

    var $monthSelect = $("select#month");
    $monthSelect.change(function() {
        var vals=$monthSelect.val().split('-');
        window.location=makeUrl("/records?year=" + vals[0] + "&month=" + vals[1]);
    });    
});