$(function() {
	$("a[data-json]").on("click", function() {
		var json = $(this).data("json");
		openDialog("Parameters", "<pre>" + JSON.stringify(json, null, '  ') + "</pre>");
		return false;
	});
})