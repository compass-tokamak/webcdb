$(function() {
	var $attachForm = $("#attach-form");

	$attachForm.submit(function() {
        if (
            parseFloat($("#coefficient_lev2V").val()) == 0.0 ||
            parseFloat($("#coefficient_V2unit").val()) == 0.0
        ) {
            return confirm("One of the coefficients is equal to zero (=> so will be the scaled data). Are you sure you want to continue?");
        } else {
            return true;
        }
    });

    /**
     * Reload board list after computer change in the attachment form.
     */
    var applyComputerChange = function() {
        $("#attach-submit").attr("disabled", true);
        var $boardSelect = $("select#board_id");
        $boardSelect.children().remove();
        $boardSelect.html("<option value='-1'>Loading...</option>");

        var ajaxOptions = {
            url : $("body").data("homepath") + "/api/get_boards",
            data : { computer_id: $("#computer_id").val() },
            dataType : "json"
        }

        var onSuccess = function(data) {
            $boardSelect.children().remove();
            for (var index in data) {
                var board = data[index];
                // console.log(board);
                var $option = $("<option value='" + board.board_id + "'>");
                $option.text("Board " + board.board_id);
                $boardSelect.append($option);
            }
            applyBoardChange();
        }
        $.ajax(ajaxOptions).done(onSuccess);
    };

    /**
     * Reload channel list after board change in the attachment form.
     */
    var applyBoardChange = function() {
        var $channelSelect = $("select#channel_id");
        $("#attach-submit").attr("disabled", true);
        
        $channelSelect.children().remove();
        $channelSelect.html("<option value='-1'>Loading...</option>");

        var ajaxOptions = {
            url : $("body").data("homepath") + "/api/get_channels",
            data : {
                computer_id: $("#computer_id").val(),
                board_id: $("#board_id").val()
            },
            dataType : "json"
        }

        var onSuccess = function(data) {
            $channelSelect.children().remove();
            if (data.length == 0) {
                $channelSelect.append($("<option value='-1'>No empty channels</option>"));
                $channelSelect.addClass("error");
            } else {
                $("#attach-submit").removeAttr("disabled");
                $channelSelect.removeClass("error");
                for (var index in data) {
                    var channel = data[index];
                    var $option = $("<option value='" + channel.channel_id + "'>");
                    $option.text("Channel " + channel.channel_id);
                    $option.data("channel_data", channel);
                    $channelSelect.append($option);
                }
            }
            applyChannelChange();
        }

        $.ajax(ajaxOptions).done(onSuccess);
    };

    var applyChannelChange = function() {
        var $select = $("select#channel_id");
        if ($select.val() != -1) {
            var $option = $select.find("option[value=" + $select.val() + "]");
            var data = $option.data("channel_data");
            if (data) {
                $("#coefficient_lev2V").val(data["coefficient_lev2V"]);
                $("#offset").val(data["offset"]);
                $("#time_axis_id").val(data["time_axis_id"]);
                return;
            }
        }
        // Fallback
        $("#coefficient_lev2V").val("1.0");
        $("#offset").val("0.0");
    };

    $("select#computer_id").change(applyComputerChange);
    $("select#board_id").change(applyBoardChange);
    $("select#channel_id").change(applyChannelChange);

    applyComputerChange();    
});