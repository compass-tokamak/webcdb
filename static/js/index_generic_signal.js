/**
 * Support javascript for /templates/generic_signals/index.html
 */
$(function() {
    var $filterButton = $("#filter-button");
    var $filterButtonText = $filterButton.find(".button-text");

    var $showAll = $("#show-all");

    var $filterForm = $("#filter-form");
    
    // $filterForm.hide();
    // $filterButton.show();

    $filterButton.click(function() {
        if ($filterForm.is(":visible")) {
            $filterForm.hide();
            $filterButtonText.text("Show filter options");
        }
        else {
            $filterForm.show();
            $filterButtonText.text("Hide filter options");
        }
    })
});