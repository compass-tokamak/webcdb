/**
 * Support javascript for computers/show
 */
 $(function() {
 	$(".board-check")
	 	.change(function() {
	 		var $table = $(this).closest("h3").siblings().filter("table");
	 		if ($(this).is(":checked")) {
	 			$table.show();
	 		} else {
	 			$table.hide();
	 		}
	 	})
	 	.trigger("change");
 });