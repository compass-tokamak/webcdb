// Storage for generic signals.
// Not filled if not requested via withGenericSignals
var genericSignals = null;

/**
  * Run something AFTER all generic signals are loaded via JSON.
  */
withGenericSignals = function(handler) {
    if (genericSignals) {
        handler(genericSignals);
        return true;
    } else {
        var url = makeUrl("/api/list_generic_signals");
        $.ajax(url)
            .done(function(data) {
                genericSignals = data;
                handler(genericSignals);
            })
            .fail(function() {
                genericSignals = null;
                handler(genericSignals);
            })
        return false;
    }
};

var dialogGenericSignalsHtml = '<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
    <div class="modal-dialog">\
      <div class="modal-content">\
        <div class="modal-header">\
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
          <h4 class="modal-title">Select generic signal</h4>\
        </div>\
        <div class="modal-body" id="gs-modal-body">\
            <div class="loading alert alert-info">Loading the generic signal list. Please wait...</div>\
        </div>\
        <div class="modal-footer">\
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>\
          <button type="button" class="btn btn-ok btn-primary">OK</button>\
        </div>\
      </div>\
    </div>\
</div>'

/**
  * Show a dialog for selecting generic signals.
  */
function dialogGenericSignals(selected, multiple, selectionHandler) {
    var $dialog = $(dialogGenericSignalsHtml);
    $("body").append($dialog);

    withGenericSignals(function(generic_signals) {
        if (!generic_signals) {
            throw "Error when loading generic signals.";
        }
        var $modalBody = $dialog.find(".modal-body");
        $modalBody.empty();
        var $form = $("<form>");
        var $filterInput = $("<input type='text' class='form-control' placeholder='Type at least 3 characters to filter signals' autofocus>");
        $form.append($filterInput);
        $modalBody.append($form);
        var $ul = $("<ul class='no-style select-gs'>");
        $.each(generic_signals, function(_, gs) {
            var $li = $("<li class='list-group-item' data-gs-id='" + gs["generic_signal_id"] 
                + "' data-gs-name='" + gs["generic_signal_name"].toLowerCase() + "' data-gs-alias='" + ("" 
                + gs["alias"]).toLowerCase() +"' data-gs='" + gs["generic_signal_id"] + "'>" 
                + gs["generic_signal_name"] + " (" + gs["data_source_name"] + ")</li>");
            $li.data("gs", gs);
            if (selected && selected.indexOf(gs["generic_signal_id"].toString()) >= 0) {
                console.log(gs);
                $li.addClass("selected");
            }
            $ul.append($li);
        });
        var $items = $ul.children();
        $items.on("click", function() {
            if (!multiple) {
                $items.not($(this)).removeClass("selected");
            }
            $(this).toggleClass("selected");
        })
        var filtered = false;
        $filterInput.on("input", function() {
            var name_part = $(this).val().toLowerCase();
            if (name_part.length) {
                $items.each(function() {
                    var $item = $(this);
                    var gs_name = $item.data("gs-name");
                    var gs_alias = $item.data("gs-alias");
                    if (gs_name && gs_name.indexOf(name_part) >= 0) {
                        $item.removeClass("filtered-out");
                    } else if (gs_alias && gs_alias.indexOf(name_part) >= 0) {
                        $item.removeClass("filtered-out");
                    } else {
                        $item.addClass("filtered-out");
                    }
                });
                filtered = true;
            } else {
                if (filtered) {
                    $items.removeClass("filtered-out");
                    filtered = false;
                }
            }
        });     
        $modalBody.append($ul);
        $button = $dialog.find(".btn-ok");
        $button.on("click", function() {
            var selected = [];
            $items.filter(".selected").each(function() {
                selected.push($(this).data("gs"));
            });
            if (selectionHandler) {
                selectionHandler(selected);
            }
            $dialog.modal("hide");
        });
    });
    $dialog.modal();
    return $dialog;
}