$(function() {
    $("#select-gs-button").click(function() {
        var $button = $(this);
        if ($button.data("dialog")) {
            $button.data("dialog").modal("show");
        } else {
            var $dialog = dialogGenericSignals(null, false, function(gs) {
                if (gs.length) {
                    var text = gs[0]["generic_signal_name"] + " (" + gs[0]["data_source_name"] + ")";
                    $("#generic_signal").text(text);
                    $("#generic_signal_id").val(gs[0]["generic_signal_id"]);
                } else {
                    $("#generic_signal").text("None selected.");
                    $("#generic_signal_id").val("");
                }
            });   
            $button.data("dialog", $dialog);
        }
    });
});