$(function() {
    var enableSignalEditing = function($element) {
        var $form = $element.find("form");
        
        var signals = $form.data("signals");
        if (signals) {
            signals = signals.toString().split(",");
        } else {
            signals = [];
        }
        
        var $editButton = $form.find(".edit-button");
        var $signalInput = $form.find("input.signal-list");

        $editButton.on("click", function() {
            var selectHandler = function(signals) {
                signals = $.map( signals, function( val, i ) {
                    return val.generic_signal_id;
                });
                $signalInput.val(signals.join(","));
                $form.submit();
            }

            dialogGenericSignals(signals, true, selectHandler);
        });
    }

    enableSignalEditing($("#inputs"));
    enableSignalEditing($("#outputs"));
});