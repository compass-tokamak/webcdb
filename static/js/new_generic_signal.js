/**
 * Support javascript for the new generic signal form.
 */
$(function() {
	$("input:checkbox").each(function () {
		var $this = $(this);
		if ($this.data("enables")) {
			var ids = $this.data("enables").split(",");
			$this.on("change", function() {
				for (var i in ids) {
					var id = ids[i];
					var on = ($this.is(":checked"));
					if (on) {
						$("#" + id).removeAttr("disabled");
						$("label[for=" + id + "]").removeClass("disabled");
					} else {
						$("#" + id).attr("disabled", "disabled");
						$("label[for=" + id + "]").addClass("disabled");
					}
					$("#" + id).trigger("change");
				}
			})
			.trigger("change");
		}
	});

	$("select").each(function() {
		var $this = $(this);
		if ($this.data("enables")) {
			var ids = $this.data("enables").split(",");
			var apply = function() {
				for (var i in ids) {
					var id = ids[i];
					var on = ($this.val() === "0") && !($this.attr("disabled"));
					if (on) {
						$("#" + id).removeAttr("disabled").show();
						$("label[for=" + id + "]").show();
					} else {
						$("#" + id).attr("disabled", "disabled").hide();
						$("label[for=" + id + "]").hide();
					}	
				}			
			}

			$this.on("change", apply);
			apply();
		}		
	});

	// Automatically prepare axis name based on the gs name
	$("#generic_signal_name").on("change", function() {
		var name = $(this).val();

		// var text = $(this).val() + "_time_axis";
		$("input").each(function() {
			var $input = $(this);
			if ($input.data("appendname")) {
				$input.val(name + $input.data("appendname"));
			}
		});
	});
});