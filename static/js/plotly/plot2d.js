$(function() {
myPlot = document.getElementById('graph');
var heatmap_points_x = 1024;
var heatmap_points_y = 768;
var contour_points_x = 50;
var contour_points_y = 50;
var n_points_x = (plot_style == "heatmap") ? heatmap_points_x : contour_points_x;
var n_points_y = (plot_style == "heatmap") ? heatmap_points_y : contour_points_y;
var min_points_to_replot = 10;
var width = 1024;
var height = 768;

////console.log(plot_style);
function plotlyPlot(req_data, do_relayout) {
    ////console.log("plotlyPlot()");
    var x = req_data['x'];
    var y = req_data['y'];
    var z = req_data['z'];
    var t = req_data['t'];
    var title = "t = "+t.toFixed(3)+"ms";
    var zmin = req_data['zmin'];
    var zmax = req_data['zmax'];
    // don't plot if no data received
    var data = {
        x: x,
        y: y,
        z: z,
        type: plot_style,
        name: 'data',
        showscale: false
    };
    // workaround the colorbar bug
    var fdata = [{
        x: [1],
        y: [1],
        z: [1],
        type: plot_style,
        name: 'data',
        showscale: false
    }]
    layout = {
        hovermode: 'closest',
        width: width,
        height: height,
        title: title,
    };
    // workaround end
    // plot graph
    //if (myPlot.hasOwnProperty("data")) $("graph").empty();
    if (!myPlot.hasOwnProperty("data")) {
        ////console.log("Plotting firstly");
        //$("#graph").html("");
        Plotly.newPlot(graph, fdata, layout);
        // on zoom
        $("#graph").bind('plotly_relayout',on_relayout);
        //custom hover
        myPlot.on('plotly_hover', function(data){
            var infotext = data.points.map(function(d){
            return ('x= '+d.x+', y= '+d.y.toPrecision(3));
            });
        })
        .on('plotly_unhover', function(data){
        });
        // on zoom
        $("#graph").bind('plotly_relayout',on_relayout);
        // workaround the colorbar bug
        d = myPlot.data.pop();
        myPlot.data.push(data);
        Plotly.redraw(myPlot);
        var update = {
            showscale: true
        };
        Plotly.restyle(myPlot, update);
        // end of workaround
    } else {
        if (x.length == 0 || y.length == 0 || z.length == 0) {
            ////console.log("No data received - skipping.");
            return false;
        }
        if (zoomHistory.length > 0) {
            ////console.log(zoomHistory[zoomHistory.length-1][0]+" "+$("#frame").val());
            if ((x.length < n_points_x/10 || x.length < min_points_to_replot) && (y.length < n_points_y/10 || y.length < min_points_to_replot) && ($("#frame").val() == zoomHistory[zoomHistory.length-1][0])) { // dont plot if data is too zoomed in.
                ////console.log(x.length + " " + y.length);
                ////console.log("Not redrawing as not enough data.");
                return;
            }
        }
        var new_data = {
            x:x,
            y:y,
            z:z,
            type: plot_style,
            name: 'data',
            showscale: false
        };
        xmin = x[0];
        xmax = x[x.length-1];
        ymin = y[0];
        ymax = y[y.length-1];
        myPlot.layout.xaxis.range = [xmin, xmax];
        myPlot.layout.yaxis.range = [ymin, ymax];
        myPlot.layout.title = title;
        data = myPlot.data.pop();
        myPlot.data.push(new_data);
        Plotly.redraw(myPlot);
        var update = {
            showscale: true
        };
        Plotly.restyle(myPlot, update);
    }
        
};

function plt(frame, xstart, xstop, ystart, ystop, format, do_relayout) {
    ////console.log("plt("+xstart+","+xstop+","+ystart+","+ystop+")");
    var url = baseUrl + "api/get_2d_json_data?" + "frame=" + frame + "&transpose=" + transpose + "&xstart=" + xstart + "&xstop=" + xstop + "&ystart=" + ystart + "&ystop=" + ystop + "&max_size_x=" + n_points_x + "&max_size_y=" + n_points_y + "&str_id=" + str_id + "[" + format + "]";
    console.log(url);
    if (parseFloat(xstop) <= parseFloat(xstart) || parseFloat(ystop) <= parseFloat(ystart)) {
        alert("Cannot plot an interval of negative or zero length.");
    } else {
        $.ajax({
            dataType: "json",
            url: url,
            success: function(data) {
                plotlyPlot(data, do_relayout);
                var current = zoomHistory.length ? zoomHistory.slice(-1)[0] : null;
                if (!current || frame != current[0] || xstart != current[1] || xstop != current[2] || ystart != current[3] || ystop != current[4]) {
                    zoomHistory.push([frame, xstart, xstop, ystart, ystop]);
                    if (zoomHistory.length == 1) {
                        $("#undo-zoom").attr("disabled", "disabled");
                    } else {
                        $("#undo-zoom").removeAttr("disabled");
                    }
                    $("#frame").val(frame);
                    $("#xstart").val(xstart);
                    $("#xstop").val(xstop);
                    $("#ystart").val(ystart);
                    $("#ystop").val(ystop);
                    location.hash = "frame=" + frame + "&xstart=" + xstart + "&xstop=" + xstop + "&ystart=" + ystart + "&ystop=" + ystop;
                }
                $("#loading").hide();
            },
            error: function() {
                $("#loading").text("Error");
            }
        });
    }
};

function replot(do_relayout) {
    ////console.log("replot()");
    var xstart = $("#xstart").val() || -1000000.0;
    var xstop = $("#xstop").val() || 1000000.0;
    var ystart = $("#ystart").val() || -1000000.0;
    var ystop = $("#ystop").val() || 1000000.0;
    var frame = $("#frame").val() || 0;
    var format = $("#viewer-format").val();
    plt(frame, xstart, xstop, ystart, ystop, format, do_relayout);
};


function on_relayout(event, eventdata) {
    //zoomInfo.innerHTML = 'ZOOM!\n' + JSON.stringify(eventdata) + '\n';
    //////console.log(event);
    //if ((!eventdata.hasOwnProperty("xaxis.range[0]") || !eventdata.hasOwnProperty("xaxis.range[1]")) && !eventdata.hasOwnProperty("xaxis.autorange")) return false; //do not restyle if caught from there
    if (((!eventdata.hasOwnProperty("xaxis.range[0]") || !eventdata.hasOwnProperty("xaxis.range[1]")) && !eventdata.hasOwnProperty("xaxis.autorange")) && ((!eventdata.hasOwnProperty("yaxis.range[0]") || !eventdata.hasOwnProperty("yaxis.range[1]")) && !eventdata.hasOwnProperty("yaxis.autorange")))  return false;
    //////console.log(eventdata);
    if (eventdata.hasOwnProperty("xaxis.range[0]") || eventdata.hasOwnProperty("yaxis.range[0]")) {
        var xstart = eventdata["xaxis.range[0]"] || $("#xstart").val();
        var xstop = eventdata["xaxis.range[1]"] || $("#xstop").val();
        var ystart = eventdata["yaxis.range[0]"] || $("#ystart").val();
        var ystop = eventdata["yaxis.range[1]"] || $("#ystop").val();
        plt($("#frame").val(), xstart, xstop, ystart, ystop, $("#viewer-format").val(), false);
    } else {
        zoomHistory = [];
        plt($("#frame").val(), -1000000, 1000000, -1000000, 1000000,  $("#viewer-format").val(),false);
    }
};


    var zoomHistory = [];
    if ($("#frame") == 0) $("prevframe").attr("disabled", "disabled");
    else $("prevframe").removeAttr("disabled");
    
    var zoomBack = function() {
        if (zoomHistory.length > 1) {
            ////console.log("zooming back");
            zoomHistory.pop();
            var coordinates = zoomHistory.pop();
            ////console.log(coordinates);
            plt(coordinates[0], coordinates[1], coordinates[2], coordinates[3], coordinates[4], $("#viewer-format").val(), true);
        }
        
//         if (coordinates[2] != null, coordinates[3] != null) {
//             var update = {
//                 'yaxis.range': [coordinates[2], coordinates[3]]
//             };
//         } else {
//             var update = {
//                 'yaxis.autorange': true
//             };
//         }
//         Plotly.relayout(graph,update);
    }

    $("#zoom").click(function() {
        replot(true);
        return false;
    });

    $('#go').click(function() {
        replot(true);
        return false;
    });
    
    $('#prevframe').click(function() {
        $("#frame").val(parseInt($("#frame").val())-1);
        replot(true);
        return false;
    });
    $('#nextframe').click(function() {
        $("#frame").val(parseInt($("#frame").val())+1);
        replot(true);
        return false;
    }); 
    
    $("#reset").click(function() {
        zoomHistory = [];
        plt($("#frame").val(),-1000000,1000000,-1000000,1000000, $("#viewer-format").val(), true);
        var update = {
            'yaxis.autorange': true
        };
        Plotly.relayout(graph,update);
        return false;
    }); 

    $("#undo-zoom").click(function() {
        zoomBack();
        return false;
    });
    $("#heatmap").click(function() {
        plot_style = 'heatmap';
        n_points_x = (plot_style == "heatmap") ? heatmap_points_x : contour_points_x;
        n_points_y = (plot_style == "heatmap") ? heatmap_points_y : contour_points_y;
        ////console.log(plot_style);
        replot(true);
    });
    $("#contour").click(function() {  
        plot_style = 'contour';
        n_points_x = (plot_style == "heatmap") ? heatmap_points_x : contour_points_x;
        n_points_y = (plot_style == "heatmap") ? heatmap_points_y : contour_points_y;
        ////console.log(plot_style);
        replot(true);
    });

    $("#transpose").change(function() {
        transpose = !transpose;
        var xstart_tmp = $('#xstart').val() || -1000000.0;
        var xstop_tmp = $('#xstop').val() || 1000000.0;
        var ystart_tmp = $('#ystart').val() || -1000000.0;
        var ystop_tmp = $('#ystop').val() || 1000000.0;
        $('#xstart').val(ystart_tmp);
        $('#xstop').val(ystop_tmp);
        $('#ystart').val(xstart_tmp);
        $('#ystop').val(xstop_tmp);
        console.log(transpose);
        replot(true);
        return false;
    });
    
    $("#download").click(function() {
        var xstart = $("#xstart").val();
        var xstop = $("#xstop").val();
        var ystart = $("#ystart").val();
        var ystop = $("#ystop").val();
        var url = baseUrl + "data_signals/" + genericSignalId + "/" + recordNumber + "/data?frame=" + $("frame").val() + "&xstart=" + xstart + "&xstop=" + xstop + "&ystart=" + ystart + "&ystop=" + ystop + "&revision=" + revision;
        $("#download").attr("href", url);
        return true;
    });

    $("#viewer-format").loadState();

    // Enable loading start and stop values from hash
    var parseHash = function(str) {
       var hash = (str ||'').replace(/^#/,'').split('&'),
           parsed = {};

       for(var i =0,el;i<hash.length; i++ ){
            el=hash[i].split('=')
            parsed[el[0]] = el[1];
       }
       return parsed;
    };
    
    var parsedLocation = parseHash(location.hash);
    if (parsedLocation.frame) {
        $("#frame").val(parsedLocation.frame);
    }
    if (parsedLocation.xstart) {
        $("#xstart").val(parsedLocation.xstart);
    }
    if (parsedLocation.xstop) {
        $("#xstop").val(parsedLocation.xstop);
    }
    if (parsedLocation.ystart) {
        $("#ystart").val(parsedLocation.ystart);
    }
    if (parsedLocation.ystop) {
        $("#ystop").val(parsedLocation.ystop);
    }
    $("#viewer-format").change(function() {
        replot(false);
    });

replot(false);


});
