$(function() {

myPlot = document.getElementById('graph');
var hoverInfo = document.getElementById('hoverinfo');
var zoomInfo = document.getElementById('zoominfo');
var n_points = 2048;
var width = 1024;
var height = 768;
  
function plotlyPlot(req_data, do_relayout) {
    ////console.log("plotlyPlot()");
    var x = req_data['x'];
    var y = req_data['y'];
    data = [{
        x: x,
        y: y,
        type: 'scatter',
        mode: 'lines',
        name: 'data'
    }];
    layout = {
        hovermode: 'closest',
        width: width,
        height: height
    };
    
    // plot graph
    if (!myPlot.hasOwnProperty("data")) {
        ////console.log("Plotting firstly");
        Plotly.newPlot(graph, data, layout);
        // on zoom
        $("#"+"graph").bind('plotly_relayout',on_relayout);
        //custom hover
        myPlot.on('plotly_hover', function(data){
            var infotext = data.points.map(function(d){
            return ('x= '+d.x+', y= '+d.y.toPrecision(3));
            });
            //hoverInfo.innerHTML = infotext.join('<br/>');
        })
        .on('plotly_unhover', function(data){
            hoverInfo.innerHTML = '';
        });
        // on zoom
        $("#"+"graph").bind('plotly_relayout',on_relayout);
        
    } else {
        ////console.log("Plotting secondly");
        
        Plotly.deleteTraces(graph,0);
        Plotly.addTraces(graph,{x:x,y:y,type:'scatter', mode:'lines'});
        // TODO: fix replotting when zoom click       
        ////console.log(do_relayout);
        if (do_relayout == true) {
            ////console.log("Click!");
            var xmin = 10000000000000000;
            var xmax = -10000000000000000;
            for (var i = 0; i < x.length; i++) {
                if (x[i] < xmin) xmin = x[i];
                if (x[i] > xmax) xmax = x[i];
            }
            ////console.log(xmin+" "+xmax);
            var update = {
                'xaxis.range': [xmin, xmax]
            };
            Plotly.relayout(graph, update);
        }
    }
        
};

function plt(start, stop, format, do_relayout, yrange) {
    ////console.log("plt("+start+","+stop+")");
    var url = baseUrl + "api/get_json_data?max_size=" + ( n_points /* width * 2 */) + "&start=" + start + "&stop=" + stop + "&str_id=" + str_id + "[" + format + "]";
    if (parseFloat(stop) <= parseFloat(start)) {
        alert("Cannot plot an interval of negative or zero length.");
    } else {
        $.ajax({
            dataType: "json",
            url: url,
            success: function(data) {
                plotlyPlot(data, do_relayout);
                var current = zoomHistory.length ? zoomHistory.slice(-1)[0] : null;
                if (!current || start != current[0] || stop != current[1]) {
                    yrange = yrange || [null, null];
                    zoomHistory.push([start, stop, yrange[0], yrange[1]]);
                    if (zoomHistory.length == 1) {
                        $("#undo-zoom").attr("disabled", "disabled");
                    } else {
                        $("#undo-zoom").removeAttr("disabled");
                    }
                    $("#start").val(start);
                    $("#stop").val(stop);
                    location.hash = "start=" + start + "&stop=" + stop;
                }
                $("#loading").hide();
            },
            error: function() {
                $("#loading").text("Error");
            }
        });
    }
};

function replot(do_relayout) {
    ////console.log("replot()");
    var start = $("#start").val() || 0.0;
    var stop = $("#stop").val() || 2000.0;
    var format = $("#viewer-format").val();
    plt(start, stop, format, do_relayout);
};


function on_relayout(event, eventdata) {
    //zoomInfo.innerHTML = 'ZOOM!\n' + JSON.stringify(eventdata) + '\n';
    ////console.log(event);
    if ((!eventdata.hasOwnProperty("xaxis.range[0]") || !eventdata.hasOwnProperty("xaxis.range[1]")) && !eventdata.hasOwnProperty("xaxis.autorange")) return false; // do not restyle if caught from there
    ////console.log(eventdata);
    if (eventdata.hasOwnProperty("xaxis.range[0]") && eventdata.hasOwnProperty("xaxis.range[1]")) {
        var start = eventdata["xaxis.range[0]"];
        var stop = eventdata["xaxis.range[1]"];
        if (eventdata.hasOwnProperty("yaxis.range[0]")) {
            var yrange = [eventdata["yaxis.range[0]"], eventdata["yaxis.range[1]"]];
        }
        plt(start, stop, $("#viewer-format").val(), false, yrange);
    } else {
        zoomHistory = [];
        plt(0, 1000000, $("#viewer-format").val(),false);
    }
};


    var zoomHistory = [];

    var zoomBack = function() {
        if (zoomHistory.length > 1) {
            // ////console.log("zooming")
            zoomHistory.pop();
            var coordinates = zoomHistory.pop();
            // ////console.log(coordinates);
            plt(coordinates[0], coordinates[1], $("#viewer-format").val(), true);
        }
        
        if (coordinates[2] != null, coordinates[3] != null) {
            var update = {
                'yaxis.range': [coordinates[2], coordinates[3]]
            };
        } else {
            ////console.log('blebleble');
            var update = {
                'yaxis.autorange': true
            };
        }
        Plotly.relayout(graph,update);
    }

    $("#zoom").click(function() {
        replot(true);
        return false;
    });

    $("#reset").click(function() {
        zoomHistory = [];
        plt(0, 1000000, $("#viewer-format").val(), true);
        var update = {
            'yaxis.autorange': true
        };
        Plotly.relayout(graph,update);
        return false;
    }); 

    $("#undo-zoom").click(function() {
        zoomBack();
        return false;
    });

    $("#download").click(function() {
        var start = $("#start").val();
        var stop = $("#stop").val();
        var url = baseUrl + "data_signals/" + genericSignalId + "/" + recordNumber + "/data?start=" + start + "&stop=" + stop + "&revision=" + revision;
        $("#download").attr("href", url);
        return true;
    });

    $("#viewer-format").loadState();

    // Enable loading start and stop values from hash
    var parseHash = function(str) {
       var hash = (str ||'').replace(/^#/,'').split('&'),
           parsed = {};

       for(var i =0,el;i<hash.length; i++ ){
            el=hash[i].split('=')
            parsed[el[0]] = el[1];
       }
       return parsed;
    };
    var parsedLocation = parseHash(location.hash);
    if (parsedLocation.start) {
        $("#start").val(parsedLocation.start)
    }
    if (parsedLocation.stop) {
        $("#stop").val(parsedLocation.stop)
    }

    $("#viewer-format").change(function() {
        replot(false);
    });

replot(false);


});
