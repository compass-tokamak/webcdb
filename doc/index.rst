.. WebCDB documentation master file, created by
   sphinx-quickstart on Wed Oct 30 10:23:54 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to WebCDB's documentation!
==================================

.. toctree::
    :maxdepth: 3

    development

Reference:
----------
.. toctree::
    :maxdepth: 3

    api/webcdb



Indices and tables:
-------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

