Development of WebCDB
=====================
A simple web interface to CDB built on `web.py framework <http://webpy.org>`_.

Requirements
------------
See requirements.txt file.

How Code is Organized
---------------------

Core Modules
~~~~~~~~~~~~
* :py:mod:`webcdb.main` - main module that is used to run the application

* :py:mod:`webcdb.auth` - authentication / authorization + custom session class

* :py:mod:`webcdb.routes` - translation of request URL to classes that respond to them

* :py:mod:`webcdb.view` - declaration of templates and namespaces for them

* :py:mod:`webcdb.config` - a few configuration variables

* :py:mod:`webcdb.lib` - a few functions that adapt pyCDB for use in WWW

* :py:mod:`webcdb.error` - standard responses after errors

Helpers
~~~~~~~
Little methods that by default are included in the template namespaces.

See :py:mod:`webcdb.helpers` module.

Models
~~~~~~
Classes that provide behaviour for the application. Each class corresponds
to one of the routes (see py:mod:`webcdb.routes`) and defines a GET or POST
method (based on what HTTP method it listens to).

Typically, each method either redirects to another URL or renders output
using one of the templates.

See :py:mod:`webcdb.models` module.

Templates
~~~~~~~~~
A directory with HTML templates that render output.

Note: the **Templetor** engine is quite well described
`here <http://www.zdrojak.cz/clanky/web-py-sablonovaci-system/>`_ (in Czech).

A subdirectory **templates/partials** contains elements that are reused across different models.

Static files
~~~~~~~~~~~~
Directory **static** contains all static files: images, stylesheets and javascripts.