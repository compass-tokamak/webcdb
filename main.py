#!/usr/bin/env python
"""
WebCDB application

This is the main file that initializes the application.
"""
import os
curdir = os.path.dirname(__file__)
import web
import sys

# Set debugging based on hostname
try:
    HOSTNAME = os.uname()[1]
except:
    import socket
    HOSTNAME = socket.gethostname()
web.config.debug = HOSTNAME not in ["webcdb", "webdaq", "cudb"]

# Routes
from routes import routes

app = web.application(routes, globals())
app.internalerror = web.debugerror

# Application configuration
import config

# Enable custom authorization
import auth
app.add_processor(auth.auth_processor)

# Enable custom session management
session_store = auth.DiskStore(os.path.join(curdir,'sessions'))
if web.config.get('_session') is None:
    session = auth.Session(app, session_store, {})
    web.config._session = session
auth.session = web.config._session

if not web.config.debug:
    sys.path.append(curdir)
    if curdir:
        os.chdir(curdir)
    os.putenv("MPLCONFIGDIR", "/tmp/")
    web.config.debug = False
    config.postproc_repository_path = "/var/www/postproc-lib"
    config.postproc_push_path = "/var/www/mercurial/postproc-lib"
else:
    config.postproc_repository_path = os.getenv("postproc_local_path")
    config.postproc_push_path = os.getenv("postproc_remote_path")

config.cwd = os.path.dirname(os.path.realpath(__file__))

# Postproc repository path
sys.path.insert(0, config.postproc_repository_path)

if __name__ == "__main__":
    # Start for local development server
    app.run()
else:
    # WSGI application for servers
    application = app.wsgifunc()

