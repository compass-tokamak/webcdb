import web
import config
import view
import auth
import os, os.path
import tempfile

def make_url(url):
    """Make a working url from the application path."""
    if url.startswith("/"):
        return web.ctx.homepath + url
    else:
        return url

def make_url_orig_file(path, url, size_threshold=100.0):
    """Returns an url for not available and small files or creates static link for larger files"""
    if not os.path.exists(path):
        return url
    if (float(os.stat(path).st_size)*2**-20) < size_threshold:
        return url
    else:
        return make_static_link(path)

def make_static_link(path):
    """Links file to /static/tmp and returns its address"""
    if not os.path.exists(path):
        return web.ctx.homepath + "/#"
    dest_dir = os.path.join(config.cwd,"static","tmp")
    if not os.path.isdir(dest_dir):
        os.makedirs(dest_dir)
    extension = os.path.splitext(path)[1]
    fid, fname = tempfile.mkstemp(dir=dest_dir,suffix=extension)
    os.remove(fname)
    os.symlink(path,fname)
    url = web.ctx.homepath + "/static/tmp/" + os.path.basename(fname)
    return url


def intranet_url(url):
    """Link intranet link with the same protocol (http/https)."""
    return web.ctx.protocol + "://" + url

def start_tag(tag_name, pair_tag=True, **kwargs):
    """Start a tag with attributes.

    Attributes are given in kwargs.
    If an attribute name starts with underscore(_), this automatically removed
    (useful for _class => class and other Python keywords.)
    """
    result = "<" + tag_name + " "
    for (key, value) in kwargs.items():
        if key[0] == "_":
            key = key[1:]
        result += web.websafe(key) + "='" + web.websafe(value) + "' "
    if not pair_tag:
        result += "/"
    result += ">"
    return result

def render_partial(name, *args, **kwargs):
    """Render a template in place.

    :param name: relative path to template without trailing .html.

    Convention: Partial template names begin with an '_'.
        If they are used in more models, they should be
        put into /partials directory.
    """
    render = view.render_plain
    for name_path in name.split("/"):
        render = getattr(render, name_path)
    return render(*args, **kwargs)

def pop_messages():
    return auth.session.pop_messages()
