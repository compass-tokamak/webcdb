def icon(icon_name):
    '''A glyph icon from bootstrap.

    See http://getbootstrap.com/components/#glyphicons for a list of available icons.
    '''
    return "<i class='glyphicon glyphicon-%s'></i>" % icon_name