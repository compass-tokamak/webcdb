"""
Template helpers for WebCDB

Various functions that can be used in templates.

They are imported and initialized in view.py.
"""
from .text import *
from .forms import *
from .base import *
from .ui import *

import web

def is_debug():
    return web.config.debug

def link(text, url, **kwargs):
    """Link

    - Relative application path is transformed into server path.
    - Kwargs are interpreted as attributes of the tag.
        * For "class" attribute, use "_class". Beginning underscore 
          is always removed to enable attributes that are keywords in Python.
    """
    result = start_tag("a", True, href=make_url(url), **kwargs)
    result += web.websafe(text) + "</a>"
    return result