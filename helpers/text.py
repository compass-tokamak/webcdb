from datetime import datetime

import web

html_escape_table = {
    "&": "&amp;",
    '"': "&quot;",
    "'": "&apos;",
    ">": "&gt;",
    "<": "&lt;",
}

def h(text):
    """Escape HTML characters.

    Note: Maybe we should use web.websafe() instead
    """
    return web.websafe(text)


def pretty_ago(time=False):
    """Pretty string like 'an hour ago', 'Yesterday', '3 months ago', 'just now', etc.

    :param time: a datetime object or an int() Epoch timestamp
    

    Copied from http://stackoverflow.com/questions/1551382/user-friendly-time-format-in-python
    """
    now = datetime.now()
    if type(time) is int:
        diff = now - datetime.fromtimestamp(time)
    elif isinstance(time,datetime):
        diff = now - time 
    elif not time:
        diff = now - now
    second_diff = diff.seconds
    day_diff = diff.days

    if day_diff < 0:
        return 'in the future'

    if day_diff == 0:
        if second_diff < 10:
            return "just now"
        if second_diff < 60:
            return str(second_diff) + " seconds ago"
        if second_diff < 120:
            return  "a minute ago"
        if second_diff < 3600:
            return str( second_diff // 60 ) + " minutes ago"
        if second_diff < 7200:
            return "an hour ago"
        if second_diff < 86400:
            return str( second_diff // 3600 ) + " hours ago"
    if day_diff == 1:
        return "Yesterday"
    if day_diff < 7:
        return str(day_diff) + " days ago"
    if day_diff < 31:
        return str(day_diff // 7) + " weeks ago"
    if day_diff < 365:
        return str(day_diff // 30) + " months ago"
    return str(day_diff // 365) + " years ago"

def month_name(year, month):
    """Month in the form of 'November 2013'"""
    months = [ "January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"]
    return "%s %d" % (months[month-1], year)



def ellipsis(text, max_length, full_title=False):
    """Shorten text if exceeding max_length and add ellipsis.

    :param text: 
    :param max_length: Length of the string to shorten to
    :param full_title: if True, wrap the text in <span> with the title of full text.

    Examples:
        ellipsis("abcdefghijk", 5) -> "abcde..."
        ellipsis("abcde", 5) -> "abcde"
        ellipsis("abcdefghijk", 5, True) -> "<span title='abcdefghijk'>abcde...</span>"
    """
    text = h( text )
    if not text:
        return ""
    elif len(text) <= max_length:
        return text
    else:
        if full_title:
            return "<span title='%s'>%s...</span>" % ( text, text[0:max_length] )
        else:
            return text[0:max_length] + "..."