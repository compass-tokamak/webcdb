import web
from .base import start_tag

def select(options, selected, **kwargs):
    """Selected from options and a selected value."""
    result = start_tag("select", True, **kwargs)
    if isinstance(options, dict):
        for (key, value) in options.items():
            if key == selected:
                result += start_tag("option", True, value=key, selected=selected)
            else:
                result += start_tag("option", True, value=key)
            result += web.websafe(value)
            result += "</option>"
    else:
        for option in options:
            if option == selected:
                result += start_tag("option", True, selected=selected)
            else:
                result += start_tag("option", True)
            result += web.websafe(option)
            result += "</option>"
    result += "</select>"
    return result

# Really used?
def form_group(label, input, input_id=None, **kwargs):
    width = kwargs.pop("width", 10)
    label_width = kwargs.pop("label_width", 12 - width)
    _class = kwargs.pop("_class", None)
    if (_class):
        _class = " ".join(_class, "form-group")
    else:
        _class = "form-group"   
    result = start_tag("div", True, _class=_class, **kwargs)
    result += start_tag("label", True, _for=input_id, _class="col-md-%d control-label" % label_width)
    result += label
    result += "</label>"
    result += "<div class='col-lg-%d'>" % width
    result += input
    result += "</div>"
    result += "</div>"

def help_button():
    """Simple help button

    On click, it should display the main page help topic.
    Implemented as helper to avoid multiple copying.
    """
    return """<button class="btn btn-info" id="help-button" type="button" title="Show help">
        <i class="glyphicon glyphicon-question-sign icon-white"></i> Help
    </button>"""