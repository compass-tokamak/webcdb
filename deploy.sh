#!/bin/sh
# This script pushes all files into the specified url.
#
# The SSH key should be generated using:
#     ssh-keygen -f deploy
# While the private key has then to be preprocessed using:
#     cat deploy | tr '\n' '_'
# and the output stored in Gitlab -> project -> Settings -> CI / CD -> Variables -> SSH_PRIVATE_KEY
# next you have to setup ~account/.ssh/authorized_keys . Consider using some constraints like:
#     from="ci-runner.tok.ipp.cas.cz,10.10.39.69",no-port-forwarding,no-X11-forwarding,no-agent-forwarding,no-pty ssh-rsa blablabla
#
# It uses sudo for some priviledge ecalation. Assuming the user is "deploy" you can pust the following lines into /etc/sudoers.d/deploy (for rsync mode):
#     deploy ALL=(ALL) NOPASSWD: /bin/systemctl start apache2.service
#     deploy ALL=(ALL) NOPASSWD: /bin/systemctl stop apache2.service
#     deploy ALL=(ALL) NOPASSWD: /bin/systemctl restart apache2.service
#     deploy ALL=(ALL) NOPASSWD: /usr/bin/chown -R www-data:www-data /destdir
#     deploy ALL=(ALL) NOPASSWD: /usr/bin/chown -R deploy /destdir
#
# Credits:
#   https://github.com/flyingshapes/gitlab-ci-rsync
#   https://www.vas-hosting.cz/blog-jak-nastavit-automaticky-deploy-aplikaci-pomoci-gitlabu#zakladninastaveni
#   https://community.netlify.com/t/support-guide-using-an-ssh-key-via-environment-variable-during-build/2457


# Some defaults
method=""
branch_or_commit="master"
chown_user=""
dest_uri=""
source_dir="./"

# writes down the private key present in environment variable, accepts destination host fingerprint
prepare_ssh() {
    ssh_host_=$1
    if [ -z "$SSH_PRIVATE_KEY" ]; then
        >&2 echo "Set SSH_PRIVATE_KEY environment variable"
        exit 1
    fi
    test -d ~/.ssh || mkdir -p ~/.ssh
    if [ ! -f ~/.ssh/id_rsa ]; then
        echo -e "${SSH_PRIVATE_KEY//_/\\n}" >> ~/.ssh/id_rsa
        chmod 600 ~/.ssh/id_rsa
    fi
    ssh-keyscan "$ssh_host_" >> ~/.ssh/known_hosts
}

# this updates using git
call_git() {
    ssh_user_host_=$1
    dest=$2
    commit=$3
    chwn=$4
    # optional chown when requested
    test -z "$chwn" || ssh $ssh_user_host "sudo chown -R \$(whoami) $dest"
    ssh $ssh_user_host git -C $dest fetch
    ssh $ssh_user_host git -C $dest checkout -f $commit
    test -z "$chwn" || ssh $ssh_user_host sudo chown -R $chwn $dest
}

# call rsync - be careful, it can eat your data
call_rsync() {
    src=$1
    ssh_user_host=$2
    dest_dir=$3
    chwn=$4
    # --rsync-path="sudo rsync" does not work here, so we run ssh chmod instead
    test -z "$chwn" || ssh $ssh_user_host "sudo /bin/chown -R \$(whoami) $dest_dir"
    echo rsync -avz -e ssh --delete --exclude-from=.gitignore --exclude-from=.deploy_ignore $src $ssh_user_host:$dest_dir
    rsync -avz -e ssh --delete --exclude-from=.gitignore --exclude-from=.deploy_ignore $src $ssh_user_host:$dest_dir
    test -z "$chwn" || ssh $ssh_user_host "sudo /bin/chown -R $chwn $dest_dir"
}

# parse command line arguments
while getopts "b:c:d:m:s:" opt
do
    case "$opt" in
        b)     branch_or_commit="$OPTARG";;
        c)     chown_user="$OPTARG";;
        d)     dest_uri="$OPTARG";;
        m)     method="$OPTARG";;
        s)     source_dir="$OPTARG";;
        [?h])   >&2 echo "Usage: $0 [-b branch_or_commit] [-c chown user] [-m method] -s source_directory -d dest_uri"
               exit 1;;
    esac
done

# process dest_uri
ssh_user_host=$(echo $dest_uri | sed 's/[:/].*//')
ssh_host=$(echo $ssh_user_host | sed 's/.*@//')
dest_dir=$(echo $dest_uri | sed '/.*:/!{q100}; s/.*://')
if [ "$?" -eq "100" ];then
    >&2 echo "destination uri parsing error, try user@host:/folder"
    exit 1
fi

# return error when dir is not properly set
if [ -z "$dest_dir" ];then
    >&2 echo "Set proper destination"
    exit 1
fi
# do not operate on /
if [ "$dest_dir" = "/" ];then
    >&2 echo "This script will not operate on /"
    exit 1
fi

# print out all parsed options
#echo "-b ${branch_or_commit}"
#echo "-c ${chown_user}"
#echo "-d ${dest_uri}"
#echo "-m ${method}"
#echo "-s ${source_dir}"
#echo "ssh_user_host: ${ssh_user_host}"
#echo "ssh_host: ${ssh_host}"
#echo "dest_dir: ${dest_dir}"

# prepare ssh
prepare_ssh ${ssh_host}

# run git or rsync
case "${method}" in
    rsync) call_rsync $source_dir $ssh_user_host $dest_dir $chown_user;;
    git) call_git $ssh_user_host $dest_dir $branch_or_commit $chown_user;;
    *) >&2 echo "Unrecognized method, use git or rsync for the method"
       exit 1;;
esac
