import web
import view
from helpers import make_url

def show404(message="Not found."):
    data = {}
    data["message"] = message
    data["referer"] = web.ctx.env.get('HTTP_REFERER', make_url("/"))
    return view.render.errors.error404( data )