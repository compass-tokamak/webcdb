import web
import view
import auth
from web import form
import lib
import error
from auth import session

axis_names = {
    "time_axis" : "time axis",
    "axis1" : "axis 1",
    "axis2" : "axis 2",
    "axis3" : "axis 3",
    "axis4" : "axis 4",
    "axis5" : "axis 5",
    "axis6" : "axis 6"
}

def _available_computers(client):
    """All computers."""
    sql = "SELECT computer_id, computer_name FROM da_computers"
    return lib.run_sql( sql, client )

class Index:
    def GET(self):
        # Result
        data = {}

        # Parse request
        defaults = {
            "name" : None,
            "data_source" : [], # https://groups.google.com/forum/?fromgroups#!topic/webpy/8VOqR12QFMM
            "attached" : False,
            "details" : False
        }
        user_data = web.input(**defaults)

        data["filters"] = {}
        data["filters"]["name"] = web.websafe(user_data.name)
        data["filters"]["data_source_ids"] = [ int(ds) for ds in user_data.data_source ]
        data["filters"]["attached"] = bool(user_data.attached)
        data["details"] = bool(user_data.details)

        # Basic SQL
        if data["details"]:
            sql = """   SELECT gs.*, sources.name as data_source_name, ca.board_id, ca.channel_id, computers.computer_name
                        FROM generic_signals AS gs
                        LEFT JOIN channel_attachments AS ca
                            ON ca.attached_generic_signal_id = gs.generic_signal_id
                        LEFT JOIN data_sources AS sources
                            ON sources.data_source_id = gs.data_source_id
                        LEFT JOIN da_computers AS computers
                            ON computers.computer_id = ca.computer_id  
                    """
        else:
            sql = """   SELECT gs.*, sources.name as data_source_name
                        FROM generic_signals AS gs
                        LEFT JOIN data_sources AS sources
                            ON sources.data_source_id = gs.data_source_id"""

        # Apply filters

        data["is_filtered"] = False
        data["data_sources"] = lib.run_sql("SELECT * FROM data_sources ORDER BY name")

        condition = " WHERE 1"
        if data["filters"]["name"]:
            data["is_filtered"] = True
            condition += " AND ( gs.generic_signal_name LIKE '%" + str(data["filters"]["name"]) + "%' OR gs.alias LIKE '%" + str(data["filters"]["name"]) + "%' )"
        if data["filters"]["data_source_ids"]:
            data["is_filtered"] = True
            condition += " AND ( gs.data_source_id IN (" + ", ".join((str(ds_id) for ds_id in data["filters"]["data_source_ids"])) + "))"
            data["filters"]["ds_str"] = ", ".join(ds["name"] for ds in data["data_sources"] if ds["data_source_id"] in data["filters"]["data_source_ids"])
        sql += condition
        sql += " ORDER BY gs.generic_signal_name"

        data["generic_signals"] = lib.run_sql(sql)
        

        return view.render.generic_signals.index( data )

class Show:
    def GET(self, *args):
        client = lib.get_client()
        try:
            generic_signal = self.get_generic_signal(*args)
        except(IndexError, TypeError):
            return error.show404("Not found.")
            
        # Data source
        try:
           generic_signal.data_source = client.get_data_source_references(data_source_id = generic_signal["data_source_id"])[0]
        except (TypeError, IndexError):
            return error.show404("Not found.")

        # Find all signals
        if generic_signal.get("wildcard", False):
            # Fake the signals when GS is wildcard
            sql = "SELECT %d AS generic_signal_id, record_number FROM shot_database ORDER BY record_number DESC" % generic_signal["generic_signal_id"]
        else:
            sql = "SELECT DISTINCT(generic_signal_id), record_number FROM data_signals WHERE generic_signal_id = %d ORDER BY record_number DESC" % generic_signal["generic_signal_id"]
        generic_signal.signals = lib.run_sql( sql, client )

        # Find all axes
        axes = { axis : client.get_generic_signal_references(generic_signal_id=generic_signal[axis + "_id"])[0] for axis in axis_names if generic_signal[axis + "_id"] is not None }
        generic_signal.axes = axes
        generic_signal.as_axis = {}
        for axis_id in axis_names:
            sql = """ SELECT gs.generic_signal_name, gs.generic_signal_id, gs.alias, sources.name FROM generic_signals AS gs 
                        LEFT JOIN data_sources AS sources ON sources.data_source_id = gs.data_source_id
                        WHERE `%s_id` = %d 
                        ORDER BY generic_signal_name
                        """ % ( axis_id, generic_signal["generic_signal_id"] )
            as_axis = lib.run_sql( sql, client )
            if as_axis:
                generic_signal.as_axis[axis_names[axis_id]] = as_axis

        # Attachment history
        history_sql = """   SELECT cs.*, comp.computer_name, MIN(shots.record_number) AS min_record, gs.generic_signal_id AS time_axis_id, gs.generic_signal_name AS time_axis_name
                            FROM channel_setup AS cs
                            JOIN da_computers AS comp
                                ON comp.computer_id = cs.computer_id
                            LEFT JOIN shot_database AS shots
                                ON shots.record_time > cs.attach_time
                            LEFT OUTER JOIN generic_signals AS gs
                                ON gs.generic_signal_id = cs.time_axis_id
                            WHERE cs.attached_generic_signal_id = %d 
                            GROUP BY cs.attach_time
                            ORDER BY cs.attach_time DESC
                            """ % generic_signal["generic_signal_id"]
        generic_signal.attachment_history = lib.run_sql(history_sql)
        for history_entry in generic_signal.attachment_history:
            if not history_entry["time_axis_name"]:
                time_axis = generic_signal.axes.get("time_axis")
                if time_axis:
                    history_entry["time_axis_id"] = time_axis["generic_signal_id"]
                    history_entry["time_axis_name"] = "%s (default)" % time_axis["generic_signal_name"]

        # Current channel setup
        attachments = client.get_attachment_table(generic_signal_id=generic_signal["generic_signal_id"])
        if attachments:
            generic_signal.attachment = attachments[0]
            if generic_signal.attachment["time_axis_id"]:
                generic_signal.axes["time_axis"] = client.get_generic_signal_references(generic_signal_id=generic_signal.attachment["time_axis_id"])[0]
            sql = "SELECT computer_name FROM da_computers WHERE computer_id = %i" % generic_signal.attachment["computer_id"]
            generic_signal.computer_name = lib.run_sql( sql, client)[0]["computer_name"]
        else:
            generic_signal.attachment = None

        parameter_sql = "SELECT * FROM signal_setup WHERE generic_signal_id=%d ORDER BY `timestamp` DESC LIMIT 1" % generic_signal.generic_signal_id
        parameter_rows = client.query(parameter_sql)
        if parameter_rows:
            generic_signal.parameters = parameter_rows[0]["parameters"]
        else:
            generic_signal.parameters = None
        return view.render.generic_signals.show( generic_signal )

class ShowById(Show):
    def get_generic_signal(self, generic_signal_id):
        client = lib.get_client()
        return client.get_generic_signal_references(generic_signal_id = int(generic_signal_id))[0]

class ShowByName(Show):
    def get_generic_signal(self, data_source_name, generic_signal_name):
        client = lib.get_client()
        return client.get_generic_signal_references("%s/%s" % (generic_signal_name, data_source_name))[0]

class Detach:
    @auth.login_required
    def POST(self, generic_signal_id):
        client = lib.get_client()
        generic_signal = client.get_generic_signal_references(generic_signal_id)[0]

        attachments = client.get_attachment_table(generic_signal_id=generic_signal["generic_signal_id"])
        if attachments:
            attachment = attachments[0]
            client.detach_channel(
                attachment["computer_id"],
                attachment["board_id"],
                attachment["channel_id"]
            )            
        else:
            # Generic signal not attached - do nothing
            pass

        session.push_message("Generic signal was detached.")
        raise web.seeother('/generic_signals/' + generic_signal_id)

class Attach:
    @auth.login_required
    def GET(self, generic_signal_id):
        """Form for attachment."""
        client = lib.get_client()
        generic_signal = client.get_generic_signal_references(generic_signal_id)[0]
        context = {}
        context["generic_signal"] = generic_signal
        context["available_computers"] = _available_computers( client )

        sql = "SELECT gs1.generic_signal_name AS name, gs1.generic_signal_id, COUNT(gs2.generic_signal_id) AS signal_count FROM generic_signals AS gs1 JOIN generic_signals AS gs2 ON gs1.generic_signal_id = gs2.time_axis_id GROUP BY gs1.generic_signal_id ORDER BY signal_count desc, name"
        context["time_axes"] = client.query(sql)
        return view.render.generic_signals.attach( context )

    @auth.login_required
    def POST(self, generic_signal_id):
        """Use form data to attach generic_signal."""
        user_data = web.input()

        computer_id = int(user_data.computer_id)
        board_id = int(user_data.board_id)
        channel_id = int(user_data.channel_id)
        coefficient_lev2V = float(user_data.coefficient_lev2V)
        offset = float(user_data.offset)
        time_axis_id = user_data.time_axis_id
        coefficient_V2unit = float(user_data.coefficient_V2unit)

        client = lib.get_client()
        generic_signal = client.get_generic_signal_references(generic_signal_id)[0]

        kwargs = {
            "offset" : offset,
            "coefficient_lev2V" : coefficient_lev2V,
            "coefficient_V2unit" : coefficient_V2unit
        }
        if time_axis_id:
            kwargs["time_axis_id"] = time_axis_id

        client.attach_channel(
            computer_id, board_id, channel_id,
            generic_signal["generic_signal_id"], 
            **kwargs
        )

        session.push_message("Generic signal was attached.")
        raise web.seeother('/generic_signals/' + generic_signal_id)

class New:
    @auth.login_required
    def GET(self):
        client = lib.get_client()
        context = {}

        # Prepare data sources
        sql = "SELECT data_source_id, name FROM data_sources"
        context["data_sources"] = client.query(sql)

        # Prepare time axes
        sql = "SELECT gs1.generic_signal_name AS name, gs1.generic_signal_id, COUNT(gs2.generic_signal_id) AS signal_count FROM generic_signals AS gs1 JOIN generic_signals AS gs2 ON gs1.generic_signal_id = gs2.time_axis_id GROUP BY gs1.generic_signal_id ORDER BY signal_count desc, name"
        context["time_axes"] = client.query(sql)

        context["axes"] = []
        for axis in range(1, 7):
            sql = "SELECT gs1.generic_signal_name AS name, gs1.generic_signal_id, COUNT(gs2.generic_signal_id) AS signal_count FROM generic_signals AS gs1 JOIN generic_signals AS gs2 ON gs1.generic_signal_id = gs2.axis%d_id GROUP BY gs1.generic_signal_id ORDER BY signal_count desc, name" % axis
            context["axes"].append(client.query(sql))

        return view.render.generic_signals.new(context)

    @auth.login_required
    def POST(self):
        # Here comes the creating of generic signal itself.
        default_values = {
            "time_axis_id" : 0,
            "enable_time_axis" : 0,
            "alias" : None,
            "description" : None,
            "time_axis_units" : 0
        }
        for i in range(1, 7):
            default_values["enable_axis" + str(i)] = None
            default_values["axis" + str(i) + "_id"] = 0
            default_values["axis" + str(i) + "_units"] = ""

        user_data = web.input(**default_values)

        generic_signal_name = web.websafe(user_data.generic_signal_name)
        alias = web.websafe(user_data.alias) or None
        data_source_id = int(user_data.data_source_id)
        units = web.websafe(user_data.units)
        signal_type = web.websafe(user_data.signal_type)
        description = web.websafe(user_data.description) or None

        enable_time_axis = bool(user_data.get("enable_time_axis"))
        time_axis_id = int(user_data.get("time_axis_id"))
        time_axis_name = web.websafe(user_data.get("time_axis_name"))
        time_axis_units = web.websafe(user_data.get("time_axis_units"))
        time_axis_signal_type = web.websafe(user_data.get("time_axis_signal_type","LINEAR"))

        # Check arguments
        if not generic_signal_name:
            raise Exception("Generic signal name is an obligatory parameter.")

        # Do it really
        client = lib.get_client()
        
        kwargs = {}

        # Create time axis if requested
        if enable_time_axis:
            if time_axis_id:
                kwargs["time_axis_id"] = time_axis_id
            else:
                client.create_generic_signal(generic_signal_name = time_axis_name, 
                    data_source_id = data_source_id, signal_type = time_axis_signal_type, units=time_axis_units)
                kwargs["time_axis_id"] = client.get_generic_signal_references(time_axis_name)[0]["generic_signal_id"]        

        # Create other axes if requested
        for i in range(0, 7):
            enable_axis = bool(user_data.get("enable_axis" + str(i)))
            if enable_axis:
                axis_arg_name = "axis" + str(i) + "_id"
                axis_id = int(user_data.get(axis_arg_name))
                axis_units = "axis" + str(i) + "_units"
                if axis_id:
                    kwargs[axis_arg_name] = axis_id
                else:
                    axis_name = user_data.get("axis" + str(i) + "_name")
                    axis_signal_type = user_data.get("axis" + str(i) + "_signal_type","LINEAR")
                    client.create_generic_signal(
                        generic_signal_name = axis_name, data_source_id = data_source_id,
                        signal_type = axis_signal_type, units=user_data.get(axis_units))
                    kwargs[axis_arg_name] = client.get_generic_signal_references(axis_name)[0]["generic_signal_id"] 

        client.create_generic_signal(generic_signal_name = generic_signal_name, alias = alias,
            data_source_id = data_source_id, units = units, description = description, signal_type = signal_type, **kwargs)     
        generic_signal_id = client.get_generic_signal_id(generic_signal_name, data_source_id)

        session.push_message("Generic signal '%s' was created." % generic_signal_name)
        raise web.seeother('/generic_signals/%d' % generic_signal_id)

class EditParameters:
    def GET(self, generic_signal_id):
        client = lib.get_client()
        try:
            generic_signal = client.get_generic_signal_references(generic_signal_id=int(generic_signal_id))[0]
        except (TypeError, IndexError):
            return error.show404("Not found.")
        parameter_sql = "SELECT * FROM signal_setup WHERE generic_signal_id=%d ORDER BY `timestamp` DESC" % generic_signal.generic_signal_id
        generic_signal.parameters = client.query(parameter_sql)
        
        return view.render.generic_signals.edit_parameters( generic_signal )

    def POST(self, generic_signal_id):
        user_data = web.input(parameters='')
        
        client = lib.get_client()
        generic_signal = client.get_generic_signal_references(generic_signal_id=int(generic_signal_id))[0]
        client.signal_setup(generic_signal_id=generic_signal.generic_signal_id, parameters=user_data.parameters)

        session.push_message("Generic signal parameters were updated.")
        raise web.seeother('/generic_signals/%s' % generic_signal_id)

class UpdateCalibration:
    @auth.login_required
    def POST(self, generic_signal_id):
        default_values = {
            "coefficient_lev2V" : 1.0,
            "offset" : 0.0,
            "coefficient_V2unit" : 1.0
        }
        user_data = web.input(**default_values)

        coefficient_lev2V = float(user_data.coefficient_lev2V)
        offset = float(user_data.offset)
        coefficient_V2unit = float(user_data.coefficient_V2unit)

        generic_signal_id = int(generic_signal_id)

        client = lib.get_client()
        client.change_calibration(generic_signal_id=generic_signal_id, coefficient_lev2V=coefficient_lev2V, offset=offset, coefficient_V2unit=coefficient_V2unit)

        session.push_message("Generic signal calibration was updated.")
        raise web.seeother('/generic_signals/%d' % generic_signal_id)
