import web
import view
import lib
import re
import error
from itertools import groupby
from lib import OrderedDict

class Index:
    def GET(self):
        client = lib.get_client()
        sql = "SELECT computers.*, daq.nodeuniqueid AS fs_node, COUNT(daq.channel_id) AS channel_count FROM da_computers AS computers LEFT JOIN DAQ_channels AS daq ON daq.computer_id = computers.computer_id GROUP BY daq.nodeuniqueid  ORDER BY computer_name"
        computers = lib.run_sql(sql)
        return view.render.computers.index( computers )

class Show:
    def GET(self, computer):
        client = lib.get_client()

        if re.match("\d+", computer):
            computer_id = int( computer )
        else:
            try:
                computer_id = client.get_computer_id( computer )
            except:
                return error.show404("Not found.")
        sql = """SELECT daq.*, ca.coefficient_lev2V as lev2V, ca.offset, ca.coefficient_V2unit as v2unit, gs.generic_signal_id, gs.generic_signal_name, ca.is_attached
            FROM DAQ_channels AS daq
            JOIN channel_attachments AS ca
                ON ca.computer_id = daq.computer_id AND ca.board_id = daq.board_id AND ca.channel_id = daq.channel_id
            JOIN generic_signals AS gs
                ON ca.attached_generic_signal_id = gs.generic_signal_id
            WHERE daq.computer_id = %d
            ORDER BY board_id, channel_id""" % computer_id
        cursor = client.db.cursor()

        try:
            cursor.execute(sql)
            channels = [client.row_as_dict(row, cursor.description) for row in cursor]
        except Exception as ex:
            cursor.close()
            raise   

        cursor.close()  

        boards = OrderedDict((board, list(channels)) for board, channels in groupby(channels, lambda channel: channel["board_id"]))

        data = {}
        data["boards"] = boards
        data["channel_count"] = len(channels)
        if data["channel_count"]:
            data["nodeuniqueid"] = channels[0]["nodeuniqueid"]
        else:
            data["nodeuniqueid"] = None
        data["computer_id"] = computer_id
        data["computer_name"] = computer

        cursor.close()  
        return view.render.computers.show( data )




