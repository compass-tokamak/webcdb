# -*- coding: utf-8 -*-
import web
import io
import tempfile
import os
import mimetypes

import lib
import view
import auth
import error

from auth import session

import numpy as np

from pyCDB import client as cdb

# Matplotlib
os.environ['MPLCONFIGDIR'] = tempfile.mkdtemp()
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg

class _Base(object):
    def _get_str_id(self, signal_ref):
        if signal_ref["variant"]:
            return "%d:%d:%s:%d" % (signal_ref["generic_signal_id"], signal_ref["record_number"], signal_ref["variant"], signal_ref["revision"])
        else:
            return "%d:%d:%d" % (signal_ref["generic_signal_id"], signal_ref["record_number"], signal_ref["revision"])

    def _get_variant_and_revision(self):
        user_data = web.input(revision = -1, variant='')
        revision = int(user_data.revision)
        variant = web.websafe(user_data.variant)
        return variant, revision

    def _get_signal_reference(self, generic_signal_id, record_number):
        """

        :rtype dict
        """
        generic_signal_id = int( generic_signal_id )
        record_number = int( record_number )

        variant, revision = self._get_variant_and_revision()
        return lib.get_client().get_signal_references(
            generic_signal_id=generic_signal_id, record_number=record_number,
            variant=variant, revision=revision
        )[0]


    def _get_signal(self, generic_signal_id, record_number):
        signal_ref = self._get_signal_reference(generic_signal_id, record_number)
        signal = lib.get_client().get_signal(signal_ref=signal_ref)
        return signal

    def _redirect_to_show(self, generic_signal_id, record_number, variant, revision=0):
        url = '/data_signals/%d/%d?variant=%s' % (int(generic_signal_id), int(record_number), web.websafe(variant))
        if revision:
            url += "&revision=%d" % revision
        web.seeother(url)

class Show(_Base):
    def GET(self, generic_signal_id, record_number ):
        record_number = int( record_number )
        generic_signal_id = int(generic_signal_id)

        variant, revision = self._get_variant_and_revision()

        context = lib.OrderedDict()
        client = lib.get_client()

        data_signals = client.get_signal_references( generic_signal_id=generic_signal_id, record_number=record_number, deleted=True)
        try:
            context.generic_signal = client.get_generic_signal_references(generic_signal_id=generic_signal_id)[0]
            context.data_source = lib.run_sql("SELECT * FROM data_sources WHERE data_source_id = %d" % context.generic_signal["data_source_id"])[0]
        except (TypeError, IndexError):
            return error.show404("Not found.")
        context.record_number = record_number

        context.variants = list(sorted({ signal["variant"] for signal in data_signals }))
        if not context.variants:
            return error.show404("Not found.")
        if variant not in context.variants:
            variant = context.variants[0]

        data_signals = [signal for signal in data_signals if signal["variant"] == variant]
        context.max_revision = max((signal["revision"] for signal in data_signals))

        if revision < 0:
            revision = context.max_revision
        data_signals = [signal for signal in data_signals if signal["revision"] == revision]

        if not data_signals:
            return error.show404("Data signal not found.")
        else:
            context.data_signal = data_signals[0]
        context.error = None

        if context.data_signal["computer_id"]:
            context.computer = lib.run_sql("SELECT * FROM da_computers WHERE computer_id = %d" % context.data_signal["computer_id"])[0]
        else:
            context.computer = None

        self._find_shots(context)

        if context.data_signal["computer_id"]:
            try:
                context.computer = lib.run_sql("SELECT * FROM da_computers WHERE computer_id = %d" % context.data_signal["computer_id"])[0]
                context.fs = lib.run_sql("SELECT nodeuniqueid, hardwareuniqueid, parameteruniqueid FROM DAQ_channels WHERE computer_id=%d AND board_id=%d AND channel_id=%d" %
                    (context.data_signal["computer_id"], context.data_signal["board_id"], context.data_signal["channel_id"])
                )[0]
            except (TypeError, IndexError):
                return error.show404("Not found.")
        else:
            context.computer = None
            context.fs = None

        self._read_data_file(context)
        self._create_str_ids(context)

        context.data_signal.parameters = None

        parameters = client.get_signal_parameters(signal_ref=context.data_signal)
        if parameters.gs_parameters or parameters.daq_parameters:
            if parameters.gs_parameters != '{}' and parameters.daq_parameters != '{}':
                context.data_signal.parameters = parameters


        # Which code to show
        context.lang = web.cookies().get("lang", "python")
        context.active_lang = { context.lang : "active"}


        return view.render.data_signals.show( context )

    def _create_str_ids(self, context):
        data_signal = context.data_signal
        generic_signal = context.generic_signal
        data_source = context.data_source
        context.strids = lib.OrderedDict()

        suffix = ":%d" % context.record_number
        if data_signal["variant"]:
            suffix += ":%s" % data_signal["variant"]
        if data_signal["revision"] != context.max_revision:
            suffix += ":%d" % data_signal["revision"]

        # Generic signals
        context.default_strid = "%s/%s" % (
            generic_signal["generic_signal_name"], data_source["name"],
        ) + suffix

        context.strids["Generic signal (name)"] = context.default_strid

        if generic_signal["alias"]:
            context.strids["Generic signal (alias)"] = "%s/%s" % (
                generic_signal["alias"], data_source["name"],
            ) + suffix

        if context.computer:
            context.strids["Hardware"] = "DAQ:%s/%s/%s" % (
                context.computer["computer_name"], data_signal["board_id"], data_signal["channel_id"]
            ) + suffix
            fs = lib.run_sql(
                "SELECT nodeuniqueid, hardwareuniqueid, parameteruniqueid FROM DAQ_channels WHERE computer_id=%d AND board_id=%d AND channel_id=%d" %
                (data_signal["computer_id"], data_signal["board_id"], data_signal["channel_id"])
            )[0]

            context.strids["FireSignal"] = "FS:%s/%s/%s" % (
                fs["nodeuniqueid"], fs["hardwareuniqueid"], fs["parameteruniqueid"]
            ) + suffix

    def _find_shots(self, context):
        context.prev_shot = lib.run_sql(
            "SELECT MAX(record_number) AS prev_shot FROM data_signals WHERE generic_signal_id={} AND record_number < {}"
                .format(context.data_signal["generic_signal_id"], context.record_number)
        )[0]["prev_shot"]

        context.next_shot = lib.run_sql(
            "SELECT MIN(record_number) AS next_shot FROM data_signals WHERE generic_signal_id={} AND record_number > {} AND record_number < 100000"
                .format(context.data_signal["generic_signal_id"], context.record_number)
        )[0]["next_shot"]

    def _read_data_file(self, context):
        client = lib.get_client()
        data_signal = context.data_signal

        if data_signal["data_file_id"]:
            context.data_file = client.get_data_file_reference(data_file_id = data_signal["data_file_id"])
            if context.data_file is None or context.data_file["data_format"] != "HDF5":
                data_signal.dimensions = None
                data_signal.rank = None
                return
            try:
                import h5py
                with h5py.File(context.data_file["full_path"], "r") as f:
                    data_signal.dimensions = f.get(context.data_signal["data_file_key"]).shape
                data_signal.dimension_string = " × ".join([str(d) for d in data_signal.dimensions])
                data_signal.rank = len([d for d in data_signal.dimensions if d > 1]) # Don't take into account trivial dimensions
            except:
                data_signal.dimensions = None
                data_signal.rank = None
                context.error = "Could not read HDF5 file: %s" % context.data_file["full_path"]
        else:
            context.data_file = None

    @auth.login_required
    def POST(self, generic_signal_id, record_number): # UPDATE
        user_data = web.input(
            variant = '',
            quality = None, note = None, offset = None, coefficient = None, coefficient_V2unit = None, time0 = None
        )

        generic_signal_id = int( generic_signal_id )
        record_number = int( record_number )

        signal_data = {}
        signal_data["sig_ref"] = self._get_signal_reference(generic_signal_id, record_number)
        signal_data["generic_signal_id"] = generic_signal_id
        signal_data["record_number"] = record_number

        signal_data["variant"] = web.websafe( user_data.variant)
        if user_data.quality is not None:
            signal_data["data_quality"] = web.websafe( user_data.quality )
        if user_data.note is not None:
            signal_data["note"] = web.websafe( user_data.note )
        if user_data.coefficient_lev2V is not None:
            signal_data["coefficient"] = float(user_data.coefficient_lev2V)
        if user_data.offset is not None:
            signal_data["offset"] = float(user_data.offset)
        if user_data.coefficient_V2unit is not None:
            signal_data["coefficient_V2unit"] = float(user_data.coefficient_V2unit)
        if user_data.time0 is not None:
            signal_data["time0"] = float(user_data.time0)
        lib.get_client().update_signal(**signal_data)

        session.push_message("Data signal was updated.")
        self._redirect_to_show(generic_signal_id, record_number, signal_data["variant"])

class Edit(_Base):
    @auth.login_required
    def GET(self, generic_signal_id, record_number):
        context = {}
        context["signal"] = self._get_signal_reference(generic_signal_id, record_number)
        return view.render.data_signals.edit( context )

class Viewer(_Base):
    def GET(self, generic_signal_id, record_number ):
        context = lib.OrderedDict()
        context.signal = self._get_signal_reference(generic_signal_id, record_number)
        try:
            context.generic_signal = lib.get_client().get_generic_signal_references( generic_signal_id = context.signal["generic_signal_id"] )[0]
        except (TypeError, IndexError):
             return error.show404("Not found.")
        context.str_id = self._get_str_id(context.signal)
        return view.render_plain.data_signals.viewer( context )

class PlotlyViewer(_Base):
    def GET(self, generic_signal_id, record_number):
        context = lib.OrderedDict()
        context.signal = self._get_signal_reference(generic_signal_id, record_number)
        try:
            context.generic_signal = lib.get_client().get_generic_signal_references( generic_signal_id = context.signal["generic_signal_id"] )[0]
        except (TypeError, IndexError):
            return error.show404("Not found.")
        context.str_id = self._get_str_id(context.signal)
        return view.render_plain.data_signals.plotly_viewer( context )

class Plotly2D(_Base):
    def GET(self, generic_signal_id, record_number, plot_type):
        context = lib.OrderedDict()
        if plot_type not in ["contour", "heatmap"]:
            raise Exception("Invalid plot type: %s" % plot_type)
        context.plot_type = plot_type
        context.signal = self._get_signal_reference(generic_signal_id, record_number)
        try:
            context.generic_signal = lib.get_client().get_generic_signal_references( generic_signal_id = context.signal["generic_signal_id"] )[0]
        except (TypeError, IndexError):
            return error.show404("Not found.")
        context.str_id = self._get_str_id(context.signal)
        return view.render_plain.data_signals.plotly_2d( context )

class Delete(_Base):
    @auth.login_required
    def GET(self, generic_signal_id, record_number):
        record_number = int(record_number)
        generic_signal_id = int(generic_signal_id)
        variant, _ = self._get_variant_and_revision()

        client = lib.get_client()
        data_signal = client.get_signal_references(generic_signal_id=generic_signal_id, record_number=record_number, variant=variant, revision=-1, deleted=True)[0]
        if not data_signal["deleted"]:
            client.delete_signal(signal_ref=data_signal)

        session.push_message("Data signal was deleted.")
        self._redirect_to_show(generic_signal_id, record_number, variant)

class Restore(_Base):
    @auth.login_required
    def GET(self, generic_signal_id, record_number):
        variant, revision = self._get_variant_and_revision()
        if revision <= 0:
            raise Exception("Invalid revision: %d" % revision)

        data_signal = self._get_signal_reference(generic_signal_id, record_number)
        if data_signal["deleted"] == 1:
            raise Exception("Cannot restore to deleted revision: %d" % revision)
        else:
            data_signal["note"] = "Restored from revision %d." % revision
            data_signal.pop("data_signal_id", None)
            data_signal.pop("revision", None)
            lib.get_client().store_signal(**data_signal)

        session.push_message("Data signal was restored to revision %d." % revision)
        self._redirect_to_show(generic_signal_id, record_number, variant)

class Undelete(_Base):
    @auth.login_required
    def GET(self, generic_signal_id, record_number):
        record_number = int(record_number)
        generic_signal_id = int(generic_signal_id)
        variant, _ = self._get_variant_and_revision()

        client = lib.get_client()
        revisions = client.get_signal_references(generic_signal_id=generic_signal_id, record_number=record_number, variant=variant)
        chosen_revision = None
        for revision in revisions:
            if ( not chosen_revision or revision["revision"] > chosen_revision["revision"] ) and not revision["deleted"]:
                chosen_revision = revision
        if not chosen_revision:
            raise Exception("It is not possible to undelete signal.")
        chosen_revision.note = "Restored from revision %d." % chosen_revision.revision
        chosen_revision.pop("data_signal_id", None)
        revision = chosen_revision.pop("revision", None)
        client.store_signal(**dict(chosen_revision))

        session.push_message("Data signal was restored to revision %d." % revision)
        self._redirect_to_show(generic_signal_id, record_number, variant)

class Data(_Base):
    def download_original(self, generic_signal_id, record_number):
        signal_reference = self._get_signal_reference(generic_signal_id, record_number)
        gs_ref = lib.get_client().get_generic_signal_references(generic_signal_id=int(generic_signal_id))[0]
        data_file = lib.get_client().get_data_file_reference(data_file_id=signal_reference.data_file_id)
        with open(data_file.full_path, 'r') as f:
           s = f.read()

        mtype, encoding = mimetypes.guess_type(data_file.full_path)
        ext = os.path.splitext(data_file.full_path)[-1]
        disposition_name = "%s-%d%s" % (gs_ref["generic_signal_name"], int(record_number), ext)
        web.header('Content-Type', mtype)
        web.header('Content-Disposition', 'attachment; filename="%s' % (disposition_name))

        return s

    def GET(self, generic_signal_id, record_number):
        user_data = web.input(format = "txt", start=0.0, stop=2000.0, separator = ' ', downsample=None, original=0)
        separator = web.websafe(user_data.separator)
        format = web.websafe(user_data.format)
        downsample = int(user_data.downsample) if user_data.downsample else None
        original = bool(user_data.original)
        if original:
            return self.download_original(generic_signal_id, record_number)
        else:
            data_signal = self._get_signal(generic_signal_id, record_number)
            data = data_signal.data

            if format in [ "txt", "matrix" ]:
                if format == "txt":
                    start = float(user_data.start) if user_data.start else None
                    stop = float(user_data.stop) if user_data.stop else None

                    if hasattr(data_signal,"time_axis"):
                        (data_signal.time_axis.data, data) = lib.get_data_slice(data_signal, start, stop, downsample=downsample)

                    # All axes
                    axes = [getattr(data_signal, ax) for ax in cdb.axes_names if hasattr(data_signal, ax)]
                    axes = [ax for ax in axes if ax is not None]

                    def _line(index):
                        value = data[index]
                        # print type(ax.data)
                        real_index = [ax.data[index[i]] for i, ax in enumerate(axes)]
                        return separator.join((str(x) for x in real_index + [value])) + "\n"
                    indices = np.ndindex(data.shape)

                elif format == "matrix":
                    if data.ndim != 2:
                        raise Exception("Matrix is available only for 2-D data.")
                    buf = io.StringIO()
                    def _line(index):
                        return separator.join((str(data[index,j]) for j in range(data.shape[1]))) + "\n"
                    indices = range(data.shape[0])

                buf = io.StringIO()
                buf.writelines((_line(index) for index in indices))
                response = buf.getvalue()

                disposition_name = data_signal.gs_ref["generic_signal_name"] + "-%d" % int(record_number) + ".txt"
                web.header('Content-Type', 'text/plain')
                web.header('Content-Length', len(response))
                web.header('Content-Disposition', 'attachment; filename="%s"' % (disposition_name))

                return response


            elif format == "hdf5":
                # :-( It is not possible to use HDF5 without an existing file
                import tempfile
                import h5py
                from pyCDB import local

                f = tempfile.NamedTemporaryFile(suffix=".h5", delete=False)
                f.close()

                local.Exporter().export_data(filename=f.name, signal=data_signal, include_meta_data=True)
                disposition_name = "%s-%d.h5" % (data_signal.gs_ref["generic_signal_name"], int(record_number))

                web.header('Content-Type', 'application/x-hdf')
                web.header('Content-Disposition', 'attachment; filename="%s' % (disposition_name))

                with open(f.name, 'rb') as f:
                    s = f.read()

                os.remove(f.name)
                return s

class Plot(_Base):
    def GET(self, generic_signal_id, record_number ):
        signal = self._get_signal(generic_signal_id, record_number)

        plt.rcParams['figure.facecolor'] = '#ffffff'

        width = int(web.websafe(web.input(width = 1024).width))
        height = int(web.websafe(web.input(height = 768).height))

        dpi = 72.

        figure = plt.figure(figsize = (width / dpi, height / dpi), dpi = dpi)
        signal.plot(fig=figure)

        canvas = FigureCanvasAgg(figure)

        # write image data to a string buffer and get the PNG image bytes
        buf = io.BytesIO()
        canvas.print_png(buf)

        data = buf.getvalue()

        web.header('Content-Type', 'image/png')
        web.header('Content-Length', len(data))

        # plot.close()
        return data
