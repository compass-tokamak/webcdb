import web
import view
import lib
import os
import config
import time
import auth
import re
from pyCDB import postproc
from multiprocessing import Process

from auth import session

# format(module, time.ctime(), method, auth.get_user())
MODULE_TEMPLATE="""'''Postprocessing module `{0}`

Created: {1} by {3}
'''
def {2}(in_refs, out_gs_ids, **kwargs):
    '''Description of the function.

    :param in_refs: list of signal references
    :param out_gs_ids: list of generic signal ids
    '''
    from pyCDB import client
    cdb = client.CDBClient() # Load PyCDB
    
    # Insert your code here.
"""
class _Base(object):
    def _get_rule(self, rule_id):
        rule_id = int(rule_id)
        rule = lib.run_sql("SELECT * FROM postproc_rules WHERE rule_id = %d" % rule_id)[0]

        try:
            rule.module_name, rule.function_name = rule["func"].split(".")
        except:
            rule.module_name = ""
            rule.function_name = ""
        return rule

    def _get_update_params(self):
        user_data = web.input(name="", description="", func="", module="")
        
        name = web.websafe(user_data.name)
        module = web.websafe(user_data.module)
        description = web.websafe(user_data.description)
        func = web.websafe(user_data.func)

        name_regexp = re.compile("^[a-zA-Z][a-zA-Z0-9_]*$")

        if not name_regexp.match(module):
            raise Exception("Module name can contain only letters, numbers & underscore and has to start with a letter.")
        if not name_regexp.match(func):
            raise Exception("Function name can contain only letters, numbers & underscore and has to start with a letter.")

        func = module + "." + func

        if not name:
            raise Exception("Rule name cannot be empty.")
        return {
            "rule_name" : name, "description" : description, "func" : func
        }

    def pull_and_update_hg(self):
        from mercurial import commands, ui, hg
        hg_ui = ui.ui()
        
        repo = hg.repository(hg_ui, config.postproc_repository_path)
        commands.pull(hg_ui, repo, config.postproc_push_path)
        commands.update(hg_ui, repo)


class Index(_Base):
    def GET(self): # LIST
        data = {}
        sql = "SELECT * FROM postproc_rules"
        rules = lib.run_sql(sql)
        data["rules"] = rules
        data["active_rules"] = [ rule for rule in rules if rule["active"] == 1]
        data["inactive_rules"] = [ rule for rule in rules if rule["active"] != 1]
        data["last_record_number"] = lib.get_client().last_record_number()
        return view.render.postprocessing.index( data )

    @auth.login_required
    def POST(self): # CREATE
        params = self._get_update_params()
        rule_id = lib.get_client().insert("postproc_rules", params, return_inserted_id=True)

        session.push_message("New rule has been created.")
        web.seeother('/postprocessing/rules/%s' % rule_id)

class Edit(_Base):
    @auth.login_required
    def GET(self, rule_id):
        rule_id = int(rule_id)
        context = {}
        context["rule"] = self._get_rule(rule_id)
        return view.render.postprocessing.edit( context )

class New(object):
    @auth.login_required
    def GET(self):
        return view.render.postprocessing.new({})

class Show(_Base):
    def GET(self, rule_id):
        rule_id = int(rule_id)
        
        context = lib.OrderedDict()
        context["rule"] = self._get_rule(rule_id)
        sql_template = """SELECT signals.*, gs.generic_signal_name, ds.name AS data_source_name
                 FROM %s AS signals
                 LEFT OUTER JOIN generic_signals AS gs
                     ON gs.generic_signal_id = signals.generic_signal_id
                 LEFT OUTER JOIN data_sources AS ds
                     ON ds.data_source_id = gs.data_source_id
                 WHERE signals.rule_id = %d"""
        context["inputs"] = lib.get_client().query(sql_template % ("rule_inputs", rule_id))
        context["outputs"] = lib.get_client().query(sql_template % ("rule_outputs", rule_id))

        context["input_list"] = ",".join([str(input.generic_signal_id) for input in context["inputs"]])
        context["output_list"] = ",".join([str(output.generic_signal_id) for output in context["outputs"]])

        log_sql = "SELECT * FROM `postproc_log` WHERE `rule_id`=%i ORDER BY `timestamp` DESC" % rule_id
        context["logs"] = lib.get_client().query(log_sql)

        return view.render.postprocessing.show( context )

    @auth.login_required
    def POST(self, rule_id): # UPDATE
        rule_id = int(rule_id)
        params = self._get_update_params()
        lib.get_client().update("postproc_rules", "rule_id", rule_id, params)
        session.push_message("Rule has been updated.")
        web.seeother('/postprocessing/rules/%s' % rule_id)

class Activate(object):
    @auth.login_required
    def POST(self, rule_id):
        rule_id = int(rule_id)
        lib.get_client().update("postproc_rules", "rule_id", rule_id, {"active" : 1})

        session.push_message("Rule has been activated.")
        web.seeother('/postprocessing/rules/%s' % rule_id)

class Deactivate(object):
    @auth.login_required
    def POST(self, rule_id):
        rule_id = int(rule_id)
        lib.get_client().update("postproc_rules", "rule_id", rule_id, {"active" : 0})

        session.push_message("Rule has been deactivated.")
        web.seeother('/postprocessing/rules/%s' % rule_id)

class Source(_Base):
    def get_file_path(self, func):
        module_name = func.split(".")[0]
        file_name = module_name + ".py"
        # TODO: check regexp
        return os.path.join(config.postproc_repository_path, file_name)

    def GET(self, rule_id):
        self.pull_and_update_hg()

        rule_id = int(rule_id)
        data = {}
        data["rule"] = lib.run_sql("SELECT * FROM postproc_rules WHERE rule_id = %d" % rule_id)[0]   
        file_path = self.get_file_path(data["rule"]["func"])
        try:
            with open(file_path) as f:
                data["new"] = False
                data["source"] = f.read()
        except IOError:
            module = data["rule"]["func"].split(".")[0]
            method = data["rule"]["func"].split(".")[1]
            data["source"] = MODULE_TEMPLATE.format(module, time.ctime(), method, auth.get_user())
            data["new"] = True
        return view.render.postprocessing.source( data )

    @auth.login_required
    def POST(self, rule_id):
        from mercurial import commands, ui, hg

        rule_id = int(rule_id)
        user_data = web.input(source="")
        source = user_data.source
        rule = lib.run_sql("SELECT * FROM postproc_rules WHERE rule_id = %d" % rule_id)[0]   
        file_path = self.get_file_path(rule["func"])
        
        # Download last version to avoid conflicts
        hg_ui = ui.ui()
        repo = hg.repository(hg_ui, config.postproc_repository_path)
        commands.pull(hg_ui, repo, config.postproc_push_path)
        commands.update(hg_ui, repo)

        try:
            with open(file_path, "w") as f:
                f.write(source)
        except IOError:
            raise

        # Commit, push & update on remote side
        commands.commit(hg_ui, repo, file_path, message="WebCDB: Edit of rule \"{}\"".format(rule["rule_name"]), addremove=True)
        commands.push(hg_ui, repo, config.postproc_push_path)
        remote_repo = hg.repository(hg_ui, config.postproc_push_path)
        commands.update(hg_ui, remote_repo)

        session.push_message("Rule source has been updated.")
        web.seeother('/postprocessing/rules/%s/source' % rule_id)

class _UpdateSignals(object):
    @auth.login_required
    def POST(self, rule_id):
        user_data = web.input(signals='')

        signals_from_request = web.websafe(user_data.signals)
        signals_from_request = signals_from_request.split(",")
        signals_from_request = [ int(signal) for signal in signals_from_request if signal]

        rule_id = int(rule_id)
        client = lib.get_client()

        signals_from_db = client.query("SELECT * FROM {} WHERE rule_id = {}".format(self.table_name, rule_id))
        signals_from_db = [ signal["generic_signal_id"] for signal in signals_from_db ]

        to_delete = [ signal for signal in signals_from_db if not signal in signals_from_request ]
        to_insert = [ signal for signal in signals_from_request if not signal in signals_from_db ]

        for signal in to_delete:
            client.delete(self.table_name, {"rule_id" : rule_id, "generic_signal_id" : signal })

        for signal in to_insert:
            client.insert(self.table_name, {"rule_id" : rule_id, "generic_signal_id" : signal })

        session.push_message("The list of signals for the rule has been updated.")
        web.seeother('/postprocessing/rules/%s' % rule_id)  

class UpdateOutputs(_UpdateSignals):
    def __init__(self, *args, **kwargs):
        super(UpdateOutputs, self).__init__(*args, **kwargs)
        self.table_name = "rule_outputs"       

class UpdateInputs(_UpdateSignals):
    def __init__(self, *args, **kwargs):
        super(UpdateInputs, self).__init__(*args, **kwargs)
        self.table_name = "rule_inputs"  

class Run(_Base):
    @auth.login_required
    def GET(self):
        self.pull_and_update_hg()
        shot = web.input(shot=None).shot
        if not shot:
            raise Exception("Invalid shot number")
        shot = int(shot)

        pp = postproc.CDBPostProc(cluster_profile=None)
        p = Process(target=pp, args=(int(shot),), kwargs={'reinit': True})

        pp._cluster_profile = 'postproc'
        p.start()

        session.push_message("Postprocessing has been queued for shot %i" % shot)

        web.seeother('/postprocessing/records/%i' % shot)

class Log(object):
    def GET(self):
        context = lib.OrderedDict()
        client = lib.get_client()
        context.logs = client.query ("SELECT * FROM postproc_run_log ORDER BY `timestamp` DESC")
        return view.render.postprocessing.log( context )

class ShowRecord(object):
    def GET(self, record_number):
        record_number = int(record_number)
        
        context = lib.OrderedDict()
        context["record_number"] = record_number

        client = lib.get_client()

        run_logs_sql = "SELECT * FROM postproc_run_log WHERE `record_number`=%i ORDER BY `timestamp` DESC" % record_number
        run_logs = client.query(run_logs_sql)
        
        run_ids = sorted({ run_log.run_id for run_log in run_logs }, reverse=True)
        context["runs"] = lib.OrderedDict()

        for run_id in run_ids:
            run = lib.OrderedDict()
            run["run_logs"] = [ run_log for run_log in run_logs if run_log["run_id"] == run_id ]

            rule_logs_sql = "SELECT * FROM `postproc_log` WHERE `run_id`=%i ORDER BY `timestamp` DESC" % run_id
            run["rule_logs"] = client.query(rule_logs_sql)

            for rule_log in run["rule_logs"]:
                rule_log.signals = (
                    (
                        pair[0],
                        client.get_generic_signal_references(generic_signal_id=pair[0])[0].generic_signal_name,
                        pair[1]
                    )
                    for pair in postproc.parse_log_sig_revs(rule_log["output_signal_revisions"])
                )
            context["runs"][run_id] = run
        return view.render.postprocessing.show_record( context )

