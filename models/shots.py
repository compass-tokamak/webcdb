import web
import lib
import view
import error
import calendar
import datetime
import itertools

class Index:
    def GET(self):
        data = {}

        today = datetime.date.today()

        user_data = web.input(filter_by="all", min=0, max=1.0e10, year=today.year, month=today.month, show_details=False )
        
        year = int(user_data.year)
        month = int(user_data.month)
        filter_by = user_data.filter_by
        data["show_details"] = user_data.show_details
        
        where = "WHERE record_type LIKE 'EXP'"
        order_by = "ORDER BY shot_database.record_number " #"DESC"

        select = """SELECT shot_database.*, COUNT(ds.generic_signal_id) AS signal_count
                    FROM shot_database
                    LEFT JOIN data_signals AS ds 
                        ON ds.record_number = shot_database.record_number
                 """
        group_by = "GROUP BY shot_database.record_number"

        min_date = lib.run_sql("SELECT MIN(record_time) AS minrt FROM shot_database WHERE record_type='EXP'")[0]["minrt"]
        if not min_date:
            min_date = datetime.datetime(2012, 1, 1)

        
        data["min"] = (min_date.year, min_date.month)
        data["max"] = (today.year, today.month)
        data["months"] = [(y, m) for y in range(data["max"][0], data["min"][0] - 1, -1)
            for m in range(12, 0, -1)
            if (y, m) >= data["min"] and (y, m) <= data["max"]
        ]

        data["year"] = year
        data["month"] = month

        data["prev"] = (year + (month - 2)  // 12, (month - 2) % 12 + 1)
        data["next"] = (year + month // 12, month % 12 + 1)
        

        begin_string = "%d-%d-01 0:00:00" % (year, month)
        end_string = "%d-%d-01 0:00:00" % (year + (month // 12), month % 12 + 1)
        where += " AND (record_time BETWEEN '%s' AND '%s')" % (begin_string, end_string)

        cal = calendar.Calendar(0)

        data["calendar"] = cal.monthdayscalendar(year, month)

        sql = "%s %s %s %s" % ( select, where, group_by, order_by )
        data["shots"] = lib.run_sql(sql)

        if today.year == year and today.month == month:
            data["today"] = today.day
        else:
            data["today"] = None

        data["day_shots"] = { day : [s for s in data["shots"] if s["record_time"].day == day] for day in cal.itermonthdays(year, month) }
        return view.render.shots.index( data )

class Show:
    def make_hardware_tree(self, context):
        computer_group_func = lambda signal: signal["computer_id"] or 0
        signal_order_func = lambda signal: (signal["computer_id"] or 0, signal["board_id"], signal["channel_id"])
        signal_key_func = lambda signal: signal["board_id"]

        sorted_signals = sorted(context["signals"], key=signal_order_func)
        grouped_by_computer = {k:list(v) for k,v in itertools.groupby(sorted_signals, computer_group_func)}
        # for k in grouped_by_computer:
        #     sorted_signals = sorted(grouped_by_computer[k], key=signal_order_func)
        #     grouped_signals = itertools.groupby(sorted_signals, signal_key_func)
        #     grouped_by_computer[k] = lib.OrderedDict(grouped_signals)
        context["grouped_signals"] = grouped_by_computer
        context["computers"] = list(grouped_by_computer.keys())

        cdb = lib.get_client()

        if context["computers"]:
            computers_sql = "SELECT computer_id, computer_name FROM da_computers WHERE computer_id IN %s" % cdb.mysql_str(context["computers"])
            context["computer_names"] = { row["computer_id"] : row["computer_name"] for row in lib.run_sql(computers_sql) }
            context["computers"].sort(key=lambda id: context["computer_names"].get(id, ''))
        else:
            context["computer_names"] = []

    def GET(self, record_number):
        context = {}

        record_number = int(record_number)

        # context["grouping_buttons"] = lib.OrderedDict([["all", "All"], ["data_sources", "By Data Sources"]])
        context["grouping_buttons"] = lib.OrderedDict([["all", "All"], ["data_sources", "By Data Sources"], ["hardware", "By Hardware"]])
        context["group_by"] = web.websafe(web.input(group_by=None).group_by or web.cookies().get("group_by") or "data_sources")
        web.setcookie("group_by", context["group_by"])

        client = lib.get_client()

        try:
            date = client.get_record_datetime(record_number = record_number)
        except:
            return error.show404("Record not found.")
        
        context["record_number"] = record_number
        context["date"] = date
        data_signals_sql = """SELECT ds.*, gs.generic_signal_name, gs.data_source_id, da.computer_name, gs_max.max_revision
                        FROM data_signals AS ds 
                        JOIN generic_signals AS gs 
                            ON ds.generic_signal_id = gs.generic_signal_id 
                        INNER JOIN (
                            SELECT generic_signal_id, max(revision) max_revision from data_signals where record_number = %d OR record_number = 0 group by generic_signal_id
                        ) as gs_max on gs.generic_signal_id = gs_max.generic_signal_id
                        LEFT JOIN da_computers AS da
                            ON da.computer_id = ds.computer_id
                        WHERE record_number = %d OR record_number = 0
                    GROUP BY generic_signal_id ORDER BY generic_signal_name
        """ % (record_number, record_number)


        context["signals"] = lib.run_sql(data_signals_sql)

        if context["group_by"] == "data_sources":
            context["data_sources"] = lib.run_sql("SELECT * FROM data_sources ORDER BY name")
            context["active_data_source_id"] = int(web.cookies().get("selected_data_source") or 0)
            context["grouped_signals"] = lib.OrderedDict()
            for data_source in context["data_sources"]:
                data_source_id = data_source["data_source_id"]
                signals = [ s for s in context["signals"] if s["data_source_id"] == data_source_id ]
                context["grouped_signals"][data_source_id] = signals

        elif context["group_by"] == "hardware":
            context["active_computer_id"] = int(web.cookies().get("active_computer_id") or 0)
            self.make_hardware_tree(context)

        records_sql = """SELECT prev, next FROM
            (SELECT max(record_number) as prev FROM shot_database WHERE record_number < %d AND record_type='EXP') AS s1
            LEFT JOIN 
            (SELECT min(record_number) as next FROM shot_database WHERE record_number > %d AND record_type='EXP') as s2
            ON 1=1
        """ % (record_number, record_number)
        context.update( lib.run_sql(records_sql)[0] )

        return view.render.shots.show(context)

class Last:
    '''Redirection to the last record'''
    def GET(self):
        client = lib.get_client()
        record_number = client.last_record_number()
        raise web.seeother('/records/%d' % record_number)

class Boards:
    def GET(self, record_number):
        record_number = int(record_number)

        data = {}

        sql = """SELECT ds.generic_signal_id, dc.computer_name, daq.board_id, daq.channel_id
                 FROM
                     DAQ_channels AS daq
                     JOIN da_computers AS dc
                        ON daq.computer_id = dc.computer_id
                     LEFT OUTER JOIN data_signals AS ds
                        ON daq.computer_id = ds.computer_id AND daq.board_id = ds.board_id AND daq.channel_id = ds.channel_id AND ds.record_number = %d
                     ORDER BY computer_name, board_id, channel_id
              """ % record_number

        channels = lib.run_sql(sql)

        data["channel_count"] = len([1 for channel in channels if channel["generic_signal_id"]])

        data["record_number"] = record_number
        data["channels"] = {}
        data["computer_names"] = sorted(set(( channel["computer_name"] for channel in channels )))

        for computer_name in data["computer_names"]:
            channels_in_computer = [ channel for channel in channels if channel["computer_name"] == computer_name]
            computer = { }

            computer["boards"] =  sorted(set(( channel["board_id"] for channel in channels_in_computer )))

            for board_id in computer["boards"]:
                channels_in_board = [ channel for channel in channels_in_computer if channel["board_id"] == board_id ]
                channels_in_board = { channel["channel_id"] : channel["generic_signal_id"] for channel in channels_in_board }
                computer[board_id] = channels_in_board
            data["channels"][computer_name] = computer

        records_sql = """SELECT MAX(smaller.record_number) AS prev, MIN(greater.record_number) AS next
                FROM shot_database AS shot
                LEFT OUTER JOIN shot_database AS smaller
                    ON smaller.record_number < shot.record_number
                LEFT OUTER JOIN shot_database AS greater
                    ON greater.record_number > shot.record_number
                WHERE shot.record_number = %d -- AND smaller.record_type='EXP' AND greater.record_type='EXP'
                GROUP BY shot.record_number""" % record_number

        data.update (lib.run_sql( records_sql)[0])

        return view.render.shots.boards(data)

