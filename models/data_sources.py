import lib
import view
import web
import auth
import error

from auth import session

class Index:
    def GET(self):
        sql = """   SELECT ds.*, COUNT(gs.generic_signal_id) AS gs_count
                    FROM data_sources AS ds
                    LEFT JOIN generic_signals AS gs
                        ON gs.data_source_id = ds.data_source_id
                    GROUP BY ds.data_source_id
                    ORDER BY ds.name
                    """
        data_sources = lib.run_sql( sql )
        return view.render.data_sources.index( data_sources )

    @auth.login_required
    def POST(self):
        user_data = web.input(name="", description="", subdirectory="")
        
        name = web.websafe(user_data.name)
        description = web.websafe(user_data.description)
        subdirectory = web.websafe(user_data.subdirectory)
        
        if not name:
            raise Exception("Data source name cannot be empty.")
        elif not subdirectory:
            raise Exception("Subdirectory cannot be empty.")

        data_source_id = lib.get_client().insert("data_sources", {
            "name" : name, "description" : description, "subdirectory" : subdirectory
        }, return_inserted_id=True)

        session.push_message("Data source '%s' was created." % name)
        web.seeother('/data_sources/%s' % name)

class Show:
    def GET(self, data_source_name):
        sql = "SELECT * FROM data_sources WHERE name = \'%s\'" % (data_source_name)
        result = lib.run_sql( sql )
        if not len(result):
            return error.show404("Not found.")
        else:
            data_source = result[0]

        data_source.generic_signals = lib.run_sql( "SELECT * FROM generic_signals WHERE data_source_id = %d ORDER BY generic_signal_name" % data_source["data_source_id"] )
        return view.render.data_sources.show( data_source )

class New:
    @auth.login_required
    def GET(self):
        return view.render.data_sources.new()