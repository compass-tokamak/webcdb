import lib
import numpy as np
import json

from .api_method import add_method, ApiMethod

class GetDataMethod(ApiMethod):
    """Method that returns array of x,y data of a signal."""
    def _fix_nan(self, point):
        point = float(point)
        if np.isnan(point):
            return None
        else:
            return point

    def _call(self, kwargs):
        client = lib.get_client()
        signal = client.get_signal(kwargs["str_id"])
        start = kwargs["start"]
        stop = kwargs["stop"]
        max_size = kwargs["max_size"]
        if not max_size or max_size > 2048:
            max_size = 2048

        (x, y) = lib.get_data_slice(signal, start, stop, max_size)

        data = [ [ float(x[i]), self._fix_nan(y[i]) ] for i in range(0, len(x)) ]
        return data

add_method(GetDataMethod("get_data",
    args={
        "str_id": str
    },
    optional_args={
        "start": float,    # Minimum in time axis
        "stop": float,     # Maximum in time axis
        "max_size": int    # Maximum number of points
    })
)

class GetJSONDataMethod(ApiMethod):
    """Method that returns JSONnified x,y data of a signal"""

    def _fix_nan2(self, x, y):
        mask = np.logical_and(~np.isnan(x),~np.isnan(y))
        xp = x[mask]
        yp = y[mask]
        return (xp, yp)

    def _call(self, kwargs):
        client = lib.get_client()
        signal = client.get_signal(kwargs["str_id"])
        start = kwargs.get("start", float('-Inf'))
        stop = kwargs.get("stop", float('Inf'))
        if np.isnan(start):
            start = float('-Inf')
        if np.isnan(stop):
            stop = float('Inf')
        max_size = kwargs["max_size"]
        if not max_size or max_size > 2048:
            max_size = 2048

        (x, y) = lib.get_data_slice(signal, start, stop, max_size)
        (x, y) = self._fix_nan2(x,y)

        #data = json.dumps(dict(x=x.tolist(), y=y.tolist()), ensure_ascii=True)
        data = dict(x=x.tolist(), y=y.tolist())

        return data


add_method(GetJSONDataMethod("get_json_data",
    args={
        "str_id": str
    },
    optional_args={
        "start": float,    # Minimum in time axis
        "stop": float,     # Maximum in time axis
        "max_size": int    # Maximum number of points
    })
)

class GetJSON2DDataMethod(ApiMethod):
    """Method that returns JSONnified x,y,z 2D data of a signal"""

    def _fix_nan(self, point):
        point = float(point)
        if np.isnan(point):
            return None
        else:
            return point

    def _call(self, kwargs):
        client = lib.get_client()
        signal = client.get_signal(kwargs["str_id"])
        frame = kwargs.get("frame", 0)
        xstart = kwargs.get("xstart", float('-Inf'))
        xstop = kwargs.get("xstop", float('Inf'))
        ystart = kwargs.get("ystart", float('-Inf'))
        ystop = kwargs.get("ystop", float('Inf'))
        if np.isnan(xstart):
            xstart = float('-Inf')
        if np.isnan(xstop):
            xstop = float('Inf')
        if np.isnan(ystart):
            ystart = float('-Inf')
        if np.isnan(ystop):
            ystop = float('Inf')
        max_size_x = kwargs.get("max_size_x",100)
        if max_size_x > 10000:
            max_size_x = 10000
        max_size_y = kwargs.get("max_size_y",100)
        if max_size_y > 10000:
            max_size_y = 10000
        #if kwargs.get("transpose","false") == "true":
            #max_size_x, max_size_y = max_size_y, max_size_x
            #print max_size_x, max_size_y 
        if kwargs.get("transpose","false") == "false":
            xstart, ystart = ystart, xstart
            xstop, ystop = ystop, xstop
        (x, y, z, t) = lib.get_data_slice_2d(signal, frame=frame, xstart=xstart, xstop=xstop, ystart=ystart, ystop=ystop, max_size_x=max_size_x, max_size_y=max_size_y)
        # Transpose by default
        if kwargs.get("transpose","false") == "false":
            x,y = y,x
            z = z.T

        zmax = -1
        zmin = 0
        if z.size:
            zmin = float(np.amin(z))
            zmax = float(np.amax(z))

        data = dict(x=x.tolist(), y=y.tolist(), z=z.tolist(), t=t, zmin=zmin, zmax=zmax)

        return data
    
    
add_method(GetJSON2DDataMethod("get_2d_json_data",
    args={
        "str_id": str
    },
    optional_args={
        "frame": int,      # Frame number
        "xstart": float,    # Minimum in x axis
        "xstop": float,     # Maximum in x axis
        "ystart": float,    # Minimum in y axis
        "ystop": float,     # Maximum in y axis
        "max_size_x": int,    # Maximum number of points in x axis
        "max_size_y": int,    # Maximum number of points in y axis
        "transpose": str
    })
)
