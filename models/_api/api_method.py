import base64
import json
import datetime
import web
import lib
import re

class ParameterMissingError(Exception):
    def __init__(self, parameter_name):
        self.parameter_name = parameter_name
        super(ParameterMissingError, self).__init__("Missing parameter: %s" % parameter_name)


class MyJsonEncoder(json.JSONEncoder):
    """Tweaked JSON encoder that can deal with all our types.

    E.g. datetime
    """
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return str(obj)
        if isinstance(obj, ApiMethod):
            return obj.__dict__
        if isinstance(obj, type):
            m = re.match("<(?:type|class) '(.*)'>", str(obj))
            return m.group(1)
        else:
            return json.JSONEncoder.default(self, obj)


class ApiMethod(object):
    """Method that can be called using API (Abstract class).

    Parameters are specified in two dictionaries: args, optional_args.
    The key is the parameter name, value is the type of the parameter.

    Normally, it returns JSON-encoded response.
    If the method is "raw" (kwarg in __init__), it returns data encoded using base64.

    You should override _call method (or use ExportedMethod).
    You can override _encode_result method (or use the default implementation).
    """

    def __init__(self, name, args = {}, optional_args = {}, **kwargs):
        self.name = name
        self.args = args
        self.optional_args = optional_args
        self.description = self.__doc__

        if "raw" in kwargs:
            self.raw = kwargs["raw"]
        else:
            self.raw = False

    def call(self):
        """Collect parameters from the request, check them and encode the result.

        This serves as a template method - uses _call and _encode_result methods.

        Missing args result in exception.
        Missing optional args are set to "None".
        """
        params = web.input()
        kwargs = {}

        content_type = web.ctx.env.get('CONTENT_TYPE')
        data = None
        if content_type and "application/json" in content_type:
            data = web.data()
            if len(data):
                data = json.loads(data)
                if not hasattr(data, "keys"):
                    data = None

        for arg in self.args.keys():
            if hasattr(params, arg):
                kwargs[arg] = self.args[arg](getattr(params, arg))
            elif data and arg in data:
                kwargs[arg] = data[arg]
            else:
                raise ParameterMissingError(arg)
        for arg in self.optional_args.keys():
            if hasattr(params, arg):
                kwargs[arg] = self.optional_args[arg](getattr(params, arg))
            elif data and arg in data:
                kwargs[arg] = data[arg]
            else:
                pass
        result = self._call(kwargs)
        return self._encode_result(result)

    def _encode_result(self, result):
        """Encode result of the methods.

        Default implementation uses JSON for objects, Base64 for binary data.
        """
        if self.raw:
            return base64.b64encode(str(result))
        else:
            return json.dumps(result, cls=MyJsonEncoder)


def add_method(method):
    """Simplify adding exported methods"""
    available_methods[method.name] = method

# All exported methods
available_methods = {} 
