import lib
from .api_method import add_method, ApiMethod
from pyCDB import fast_client

fast_client = fast_client.FastClient()      # One for all calls (it is thread safe)


class FastStoreMethod(ApiMethod):
    """Method that directly maps to the CDBClient method with the same name."""

    def __init__(self, name, args = {}, optional_args = {}, **kwargs):
        super(FastStoreMethod, self).__init__(name, args, optional_args, **kwargs)
        self.description = "Mapped PyCDB.fast_client.FastClient method `{}` (see pyCDB documentation).".format(self.name)

    def _call(self, kwargs):
        method_to_call = getattr(fast_client, self.name)
        return method_to_call(**kwargs)


#provide computer_id, board_id, channel_id or computer_name, board_id, channel_id or name
add_method(FastStoreMethod("new_data_fileF",
    args={
        "collection_name": str,
        "record_number": int
    },
    optional_args={
        "computer_id": int,
        "computer_name": str,
        "board_id": int,
        "channel_id": int,
        "name": str,
        "data_root": str,
        "file_format": str,
        "file_extension": str,
        "create_subdir": bool
    })
)

add_method(FastStoreMethod("set_file_readyF",
    args={
        "data_file_id": int
    },
    optional_args={
        "chmod_ro": int
    })
)

#provide computer_id, board_id, channel_id or computer_name, board_id, channel_id or name
add_method(FastStoreMethod("store_signalF",
    args={
        "record_number": int,
        "signal_type": str
    },
    optional_args={
        "computer_id": int,
        "computer_name": str,
        "board_id": int,
        "channel_id": int,
        "name": str,
        "time0": float,
        "offset": float,
        "coefficient": float,
        "coefficient_V2unit": float,
        "data_file_key": str,
        "data_file_id" : int,
        "time_axis_id" : int,
        "time_axis_coefficient" : float,
        "time_axis_revision" : int,
        "note" : str,
        "axis1_revision" : str,
        "axis2_revision" : str,
        "axis3_revision" : str,
        "axis4_revision" : str,
        "axis5_revision" : str,
        "axis6_revision" : str,
        "time_axis_variant" : str,
        "axis1_variant" : str,
        "axis2_variant" : str,
        "axis3_variant" : str,
        "axis4_variant" : str,
        "axis5_variant" : str,
        "axis6_variant" : str,
        "data_quality" : str,
        "daq_parameters" : str,
        "ignore_if_exists": bool
        # "gs_parameters": str,
        # "set_file_ready": bool
    })

)

add_method(FastStoreMethod("create_recordF",
    args={
        "record_type": str,
        "description": str,
        "ControlString": str
    })
)

add_method(FastStoreMethod("last_record_numberF",
    optional_args={
        "record_type": str
    })
)