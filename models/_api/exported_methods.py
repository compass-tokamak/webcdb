import lib
from .api_method import add_method, ApiMethod

class ExportedMethod(ApiMethod):
    """Method that directly maps to the CDBClient method with the same name."""

    def __init__(self, name, args = {}, optional_args = {}, **kwargs):
        super(ExportedMethod, self).__init__(name, args, optional_args, **kwargs)
        self.description = "Mapped PyCDB method `{}` (see pyCDB documentation).".format(self.name)

    def _call(self, kwargs):
        client = lib.get_client()
        method_to_call = getattr(client, self.name)
        return method_to_call(**kwargs)


add_method(ExportedMethod("last_shot_number"))

add_method(ExportedMethod("get_signal_references",
    args = { },
    optional_args={
        "str_id" : str,
        "generic_signal_id" : int,
        "record_number" : int,
        "revision" : int,
        "dereference" : bool
    })
)

add_method(ExportedMethod("get_attachment_table",
    args={},
    optional_args={
        # "timestamp: reference time (default current)
        "computer_id": int,
        "computer_name": str,
        "board_id": int,
        "channel_id": int,
        "generic_signal_id": int,
        "record_number": int
    })
)

add_method(ExportedMethod("get_generic_signal_references",
    args={},
    optional_args={
        "generic_signal_id": int,
        "str_id": str,
        "alias_or_name": str,
        "generic_signal_name": str,
        "data_source_id": int
    })
)

add_method(ExportedMethod("get_signal_setup",
    args={},
    optional_args={
        # "timestamp: reference time (default current)
        "gs_str_id": str,
        "record_number": int
    })
)

add_method(ExportedMethod("find_generic_signals",
    args={
        "alias_or_name": str
    },
    optional_args={
        "regexp": int
    })
)

add_method(ExportedMethod("new_data_file",
    args={
        "collection_name": str,
        "record_number": int,
        "data_source_id": int
    },
    optional_args={
        "data_root": str
    })
)

add_method(ExportedMethod("set_file_ready",
    args={
        "data_file_id": int
    },
    optional_args={
        "chmod_ro": int
    })
)

add_method(ExportedMethod("store_signal",
    args={
        "generic_signal_id": int,
        "record_number": int
    },
    optional_args={
        "time0": float,
        "offset": float,
        "coefficient": float,
        "coefficient_V2unit": float,
        "data_file_key": str,
        "data_file_id" : int,
        "computer_id" : int,
        "computer_name": str,
        "board_id" : int,
        "channel_id" : int,
        "time_axis_id" : int,
        "time_axis_revision" : int,
        "daq_parameters" : str,
        "ignore_if_exists": bool
    })
)

add_method(ExportedMethod("FS2CDB_ref",
    args={
        "nodeuniqueid" : str,
        "hardwareuniqueid" : str,
        "parameteruniqueid" : str
    })
)

add_method(ExportedMethod("get_data_file_reference",
    args = {"data_file_id": int },
    optional_args={})
)
