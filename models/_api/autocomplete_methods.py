import lib
from .api_method import add_method, ApiMethod

class GetBoardsMethod(ApiMethod):
    '''All boards for a computer.'''
    def _call(self, kwargs):
        if "computer_name" in kwargs:
            sql = """SELECT DISTINCT(board_id), hardwareuniqueid FROM DAQ_channels 
                         INNER JOIN da_computers ON DAQ_channels.computer_id=da_computers.computer_id
                         WHERE computer_name = \"%s\" ORDER BY board_id""" % lib.pyCDBBase.escape_string(kwargs["computer_name"])
            return lib.run_sql(sql)
        elif "computer_id" in kwargs:
            sql = "SELECT DISTINCT(board_id), hardwareuniqueid FROM DAQ_channels WHERE computer_id = %d ORDER BY board_id" % kwargs["computer_id"]
            return lib.run_sql(sql)
        return "Missing parameter:  computer_id or computer_name has to be provided"

class GetChannelsMethod(ApiMethod):
    '''All empty channels for a board.'''
    def _call(self, kwargs):
        if "computer_name" in kwargs:
            sql = """SELECT DISTINCT(daq.channel_id), daq.parameteruniqueid, attach.coefficient_lev2V, attach.offset, def_gs.time_axis_id
                 FROM DAQ_channels AS daq
                 JOIN channel_attachments AS attach
                    ON daq.computer_id = attach.computer_id AND daq.board_id = attach.board_id AND daq.channel_id = attach.channel_id
                 JOIN generic_signals AS def_gs
                    ON def_gs.generic_signal_id = attach.default_generic_signal_id
                 JOIN da_computers as dac
                    ON  daq.computer_id = dac.computer_id
                 WHERE dac.computer_name = \"%s\" AND daq.board_id = %d AND attach.is_attached = 0
                 ORDER BY channel_id""" % ( lib.pyCDBBase.escape_string(kwargs["computer_name"]), kwargs["board_id"] )
            return lib.run_sql(sql)
        elif "computer_id" in kwargs:
            sql = """SELECT DISTINCT(daq.channel_id), daq.parameteruniqueid, attach.coefficient_lev2V, attach.offset, def_gs.time_axis_id
                 FROM DAQ_channels AS daq
                 JOIN channel_attachments AS attach
                    ON daq.computer_id = attach.computer_id AND daq.board_id = attach.board_id AND daq.channel_id = attach.channel_id
                 JOIN generic_signals AS def_gs
                    ON def_gs.generic_signal_id = attach.default_generic_signal_id
                 WHERE daq.computer_id = %d AND daq.board_id = %d AND attach.is_attached = 0
                 ORDER BY channel_id""" % ( kwargs["computer_id"], kwargs["board_id"] )
            return lib.run_sql(sql)
        else:
            return "Missing parameter:  computer_id or computer_name has to be provided"
class ListGenericSignals(ApiMethod):
    '''List of all generic signals with id and data source in alphabetical order.

    It is possible to include info about how often the signal is used as axis
    for other signals.
    '''
    def _call(self, kwargs):
        if not kwargs.get("include_axes"):
            sql = """SELECT gs.generic_signal_id, gs.generic_signal_name, gs.alias, gs.data_source_id, ds.name AS data_source_name
                     FROM generic_signals AS gs
                     JOIN data_sources AS ds
                        ON ds.data_source_id = gs.data_source_id
                     ORDER BY gs.generic_signal_name, gs.data_source_id
                  """
        else:
            sql = """SELECT gs.generic_signal_id, gs.generic_signal_name, gs.alias, gs.data_source_id, ds.name AS data_source_name,
                        COUNT(gs_ta.generic_signal_id) AS time_axis_count, COUNT(gs_a1.generic_signal_id) AS axis1_count,
                        COUNT(gs_a2.generic_signal_id) AS axis2_count, COUNT(gs_a3.generic_signal_id) AS axis3_count,
                        COUNT(gs_a4.generic_signal_id) AS axis4_count, COUNT(gs_a5.generic_signal_id) AS axis5_count,
                        COUNT(gs_a6.generic_signal_id) AS axis6_count
                     FROM generic_signals AS gs
                     JOIN data_sources AS ds
                        ON ds.data_source_id = gs.data_source_id
                     LEFT JOIN generic_signals AS gs_a1
                        ON gs_a1.axis1_id = gs.generic_signal_id
                     LEFT JOIN generic_signals AS gs_a2
                        ON gs_a2.axis2_id = gs.generic_signal_id
                     LEFT JOIN generic_signals AS gs_a3
                        ON gs_a3.axis3_id = gs.generic_signal_id
                     LEFT JOIN generic_signals AS gs_a4
                        ON gs_a4.axis4_id = gs.generic_signal_id
                     LEFT JOIN generic_signals AS gs_a5
                        ON gs_a5.axis5_id = gs.generic_signal_id
                     LEFT JOIN generic_signals AS gs_a6
                        ON gs_a6.axis6_id = gs.generic_signal_id
                     LEFT JOIN generic_signals AS gs_ta
                        ON gs_ta.time_axis_id = gs.generic_signal_id
                     GROUP BY gs.generic_signal_id
                     ORDER BY gs.generic_signal_name, gs.data_source_id
                  """
        return lib.run_sql(sql)

add_method(GetBoardsMethod("get_boards",
    args={},
    optional_args={
      "computer_id": int,
      "computer_name": str
    })
)

add_method(GetChannelsMethod("get_channels",
    args={
        "board_id" : int
    },
    optional_args={
        "computer_id": int,
        "computer_name": str
})
)

add_method(ListGenericSignals("list_generic_signals",
    args={},
    optional_args={
        "include_axes": bool
    })
)
