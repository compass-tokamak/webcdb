import lib
import view
import auth
import web
import error
from auth import session

class _Base(object):
    def get_channel(self, computer_name, board_id, channel_id, include_history=True, include_signals=True, min_shot=None, max_shot=None):
        client = lib.get_client()
        computer_id = client.get_computer_id(computer_name=computer_name)

        board_id = int(board_id)
        channel_id = int(channel_id)
 
        sql = "SELECT * FROM DAQ_channels WHERE computer_id=%d AND board_id = %d AND channel_id = %d" % (computer_id, board_id, channel_id)
        channel = lib.run_sql(sql)[0]
        channel["computer_name"] = computer_name

        # Collected data (excluding deleted)
        if include_signals:
            query = "SELECT record_number, generic_signal_id, revision, variant, deleted FROM data_signals WHERE computer_id={} AND board_id={} AND channel_id={}"
            query = query.format(computer_id, board_id, channel_id)
            if min_shot and max_shot:
                query += " AND (record_number BETWEEN {} AND {})".format(int(min_shot), int(max_shot))
            signals = client.query(query)

            channel.signals = tuple(reversed(client.get_max_revisions(signals, deleted=False)))

        # Attachment history
        if include_history:
            history_sql = """SELECT
                cs.*, gs.generic_signal_name, gs.generic_signal_id, MIN(shots.record_number) AS min_record
                FROM channel_setup AS cs
                JOIN generic_signals AS gs
                    ON cs.attached_generic_signal_id = gs.generic_signal_id
                LEFT JOIN shot_database AS shots
                    ON shots.record_time > cs.attach_time
                WHERE computer_id=%d AND board_id = %d AND channel_id = %d
                GROUP BY cs.attach_time
                ORDER BY cs.attach_time DESC
                """  % (computer_id, board_id, channel_id)
            channel.attachment_history = lib.run_sql(history_sql)
            if channel.attachment_history:
                channel.current_attachment = channel.attachment_history[0]
            else:
                channel.current_attachment = None
        return channel

class Show(_Base):
    def GET(self, computer_name, board_id, channel_id):
        context = lib.OrderedDict()
        try:
            context.channel = self.get_channel(computer_name, board_id, channel_id)
        except (IndexError,TypeError):
            return error.show404("Not found.")
        return view.render.channels.show( context )

class EditHistory(_Base):
    @auth.login_required
    def GET(self, computer_name, board_id, channel_id):
        channel = self.get_channel(computer_name, board_id, channel_id, include_signals=False)
        return view.render.channels.edit_history( channel )

    @auth.login_required
    def POST(self, computer_name, board_id, channel_id):
        default_values = {
            "coefficient_lev2V" : None,
            "offset" : None,
            "coefficient_V2unit" : None,
            "note" : None,
            "first": None,
            "last": None,
            "generic_signal_id": None
        }
        user_data = web.input(**default_values)
        
        if not user_data.first or not user_data.last:
            raise Exception("You have to specify first and last record.")
        else:
            record_range = range(int(user_data.first), int(user_data.last) + 1)

        channel = self.get_channel(computer_name, board_id, channel_id, include_history=False, min_shot=int(user_data.first), max_shot=int(user_data.last))
        kwargs = dict( computer_id=channel["computer_id"], board_id=channel["board_id"], channel_id=channel["channel_id"] )

        if user_data.coefficient_lev2V is not None:
            kwargs["coefficient"] = float(user_data.coefficient_lev2V)
        if user_data.offset is not None:
            kwargs["offset"] = float(user_data.offset)
        if user_data.coefficient_V2unit is not None:
            kwargs["coefficient_V2unit"] = float(user_data.coefficient_V2unit)
        if user_data.note is not None:
            kwargs["note"] = user_data.note

        updated_records = []

        client = lib.get_client()
        for signal in channel.signals:
            updated_records.append(signal["record_number"])
            generic_signal_id = int(user_data.generic_signal_id or signal["generic_signal_id"])
            client.store_channel_data_as_signal(generic_signal_id=generic_signal_id, record_numbers=signal["record_number"], **kwargs)

        session.push_message("Channel was updated for records: " + ", ".join(str(record) for record in sorted(updated_records)) + ".") 
        raise web.seeother('/hardware/%s/%s/%s' % (computer_name, board_id, channel_id))

class EditParameters(_Base):
    def GET(self, computer_name, board_id, channel_id):
        context = lib.OrderedDict()
        context.channel = self.get_channel(computer_name, board_id, channel_id)
        return view.render.channels.edit_parameters( context )

    @auth.login_required
    def POST(self, computer_name, board_id, channel_id):
        channel = self.get_channel(computer_name, board_id, channel_id)
        user_data = web.input(parameters='')
        lib.get_client().change_calibration(
            computer_id=channel.computer_id,
            board_id=int(board_id),
            channel_id=int(channel_id),
            parameters=user_data.parameters
        )
        session.push_message("Channel parameters were updated.")
        raise web.seeother('/hardware/%s/%s/%s' % (computer_name, board_id, channel_id))