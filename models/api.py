"""
Public JSON-based API (~web services)
=====================================
Protocol:
---------
- JSON (MyJsonEncoder provides serialization)
- for binary data (raw methods), Base64 is used
- GET and POST are supported in the same way

Methods:
--------
All executable methods are stored in available_methods dict (use "add_method" method).

- Usually, the API just wraps underlying CDBClient methods (ExportedMethod class)
- There are a few more methods though. For these, you have to provide your own class
  that implements call() method and is added to available_methods. 

All methods (and functionality) are stored in _api module.
"""

import traceback
import json
import web

from ._api import available_methods, MyJsonEncoder, ParameterMissingError

class Index:
    """List of all method that are callable using Web API."""
    def GET(self):
        try:
            output = json.dumps(available_methods, cls=MyJsonEncoder)
            web.header('Content-type', 'application/json') 
            return output
        except:
            raise

class Execute:
    """Execute a method."""
    def _execute(self, method):
        try:
            available_method = available_methods[method]
        except KeyError:
            web.ctx.status = '404 Not Found'
            s = "Method not available: %s\n" % method
            s += "Available methods: " + ",".join(available_methods.keys()) 
            return s

        try:            
            output = available_method.call()
            if not available_method.raw:
                web.header('Content-Type', 'application/json')
            return output
        except ParameterMissingError as e:
            web.ctx.status = '400 Bad request'
            return "Missing parameter: %s" % e.parameter_name
        except Exception:
            response = "Server error\n------------\n"
            web.ctx.status = '500 Internal Server Error'
            response += traceback.format_exc()
            return response

    def GET(self, method):
        return self._execute(method)

    def POST(self, method):
        return self._execute(method)
