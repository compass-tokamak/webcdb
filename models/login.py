import web
import auth
from auth import session

class Login(object):
    '''The only page that requires authentication in the local network.'''
    def GET(self):
        if not auth.authenticate():
            return auth.send_unauthorized()
        else:
            path = session.get("redir_path") or "/"
            session.push_message("User '%s' logged in." % session.user)
            session["redir_path"] = None
            raise web.seeother(path)

class Logout(object):
    def GET(self):
        auth.session.user = None
        session.push_message("Logged out.")
        raise web.seeother('/')
