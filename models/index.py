import view
import lib

class Index:
    def GET(self):
        """Home page of the WebCDB application."""
        client = lib.get_client()
        record_number = client.last_record_number()

        context = {}
        context["record_number"] = record_number
        context["record_date"] = client.get_record_datetime(record_number=record_number)

        return view.render.index(context)